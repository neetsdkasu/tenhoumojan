VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPrivateProfileString"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

#Const conDebug = -1

Private Declare Function GetPrivateProfileString _
    Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpDefault As String, _
        ByVal lpReturnedString As String, _
        ByVal nSize As Long, _
        ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString _
    Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpString As Any, _
        ByVal lpFileName As String) As Long


Dim mlngStringLength        As Long
Dim mstrFilePath            As String
Dim mblnNoPath              As Boolean

Public Property Get StringLength() As Long
    StringLength = mlngStringLength
End Property

Public Property Get FilePath() As String
    FilePath = mstrFilePath
End Property

Public Function SetFilePath(strFilePath As String) As Boolean
    mstrFilePath = Trim(strFilePath)
    mblnNoPath = (Len(mstrFilePath) = 0)
    SetFilePath = mblnNoPath
End Function

Public Function SetStringLength(lngNewLength As Long) As Long
    mlngStringLength = IIf(lngNewLength < 50, 50, lngNewLength)
    SetStringLength = mlngStringLength
End Function

Public Function WriteString( _
                ByRef strAppName As String, _
                ByRef strKeyName As String, _
                ByRef strData As String) As Long
    
    If mblnNoPath Then
        WriteString = 0
        Exit Function
    End If
    
    Dim lngResult   As Long
    
    On Error Resume Next
    
    lngResult = WritePrivateProfileString( _
                    strAppName, _
                    strKeyName, _
                    strData, _
                    mstrFilePath)
    
    If (Err.Number <> 0) Or (lngResult = 0) Then
        WriteString = 0
        #If conDebug Then
            Debug.Print Now; "Error(clsPrivateProfileString.WriteString: "; Err.Number; ") "
            Debug.Print "Description: "; Err.Description
            Debug.Print "HelpContext: "; Err.HelpContext
            Debug.Print "HelpFile: "; Err.HelpFile
            Debug.Print "LastDllError: "; Err.LastDllError
            Debug.Print "Source: "; Err.Source
            Debug.Print "Param1(strAppName): "; strAppName
            Debug.Print "Param2(strKeyName): "; strKeyName
            Debug.Print "Param3(strData): "; strData
            Debug.Print "Other(mstrFilePath): "; mstrFilePath
            Debug.Print "Other(lngResult): "; lngResult
        #End If
        Err.Clear
        Exit Function
    End If
    
    WriteString = lngResult
    
End Function

Public Function GetAppList(ByRef strReturned() As String) As Long
    If mblnNoPath Then
        GetAppList = 0
        Exit Function
    End If
        
    Dim strData     As String * 1024
    Dim lngResult   As Long
    
    strData = String(mlngStringLength, vbNullChar)
    
    On Error Resume Next
    
    lngResult = GetPrivateProfileString( _
                    vbNullString, _
                    vbNullString, _
                    vbNullString, _
                    strData, _
                    1024, _
                    mstrFilePath)

    If Err.Number <> 0 Then
        GetAppList = 0
        #If conDebug Then
            Debug.Print Now; "Error(clsPrivateProfileString.GetAppList: "; Err.Number; ") "
            Debug.Print "Description: "; Err.Description
            Debug.Print "HelpContext: "; Err.HelpContext
            Debug.Print "HelpFile: "; Err.HelpFile
            Debug.Print "LastDllError: "; Err.LastDllError
            Debug.Print "Source: "; Err.Source
            Debug.Print "Other(mstrFilePath): "; mstrFilePath
            Debug.Print "Other(lngResult): "; lngResult
        #End If
        Err.Clear
        Exit Function
    End If
    
    On Error GoTo 0
    
    If lngResult = 0 Then
        GetAppList = 0
        Erase strReturned
        Exit Function
    End If
    
    Dim i&, n&
    Dim sp$()
    
    ReDim strReturned(0)
    
    sp = Split(strData, vbNullChar)
    
    For i = 0 To UBound(sp)
        If sp(i) = "" Then
            Exit For
        End If
        ReDim Preserve strReturned(n)
        strReturned(n) = sp(i)
        n = n + 1
    Next i
    
    GetAppList = n
    
End Function

Public Function GetKeyList(ByRef strAppName As String, _
                            ByRef strReturned() As String) As Long
    If mblnNoPath Then
        GetKeyList = 0
        Exit Function
    End If
        
    Dim strData     As String * 1024
    Dim lngResult   As Long
    
    strData = String(mlngStringLength, vbNullChar)
    
    On Error Resume Next
    
    lngResult = GetPrivateProfileString( _
                    strAppName, _
                    vbNullString, _
                    vbNullString, _
                    strData, _
                    1024, _
                    mstrFilePath)

    If Err.Number <> 0 Then
        GetKeyList = 0
        #If conDebug Then
            Debug.Print Now; "Error(clsPrivateProfileString.GetKeyList: "; Err.Number; ") "
            Debug.Print "Description: "; Err.Description
            Debug.Print "HelpContext: "; Err.HelpContext
            Debug.Print "HelpFile: "; Err.HelpFile
            Debug.Print "LastDllError: "; Err.LastDllError
            Debug.Print "Source: "; Err.Source
            Debug.Print "Param1(strAppName): "; strAppName
            Debug.Print "Other(mstrFilePath): "; mstrFilePath
            Debug.Print "Other(lngResult): "; lngResult
        #End If
        Err.Clear
        Exit Function
    End If
    
    On Error GoTo 0
    
    If lngResult = 0 Then
        GetKeyList = 0
        Erase strReturned
        Exit Function
    End If
    
    Dim i&, n&
    Dim sp$()
    
    ReDim strReturned(0)
    
    sp = Split(strData, vbNullChar)
    
    For i = 0 To UBound(sp)
        If sp(i) = "" Then
            Exit For
        End If
        ReDim Preserve strReturned(n)
        strReturned(n) = sp(i)
        n = n + 1
    Next i
    
    GetKeyList = n
    
End Function

Public Function GetString( _
                ByRef strAppName As String, _
                ByRef strKeyName As String, _
                ByRef strDefault As String, _
                ByRef strReturned As String) As Long
    
    If mblnNoPath Then
        GetString = -1
        strReturned = ""
        Exit Function
    End If
    
    Dim strData     As String
    Dim lngResult   As Long
    
    strData = String(mlngStringLength, vbNullChar)
    
    On Error Resume Next
    
    lngResult = GetPrivateProfileString( _
                    strAppName, _
                    strKeyName, _
                    strDefault, _
                    strData, _
                    mlngStringLength, _
                    mstrFilePath)
   
    If Err.Number <> 0 Then
        GetString = -2
        strReturned = ""
        #If conDebug Then
            Debug.Print Now; "Error(clsPrivateProfileString.GetString: "; Err.Number; ") "
            Debug.Print "Description: "; Err.Description
            Debug.Print "HelpContext: "; Err.HelpContext
            Debug.Print "HelpFile: "; Err.HelpFile
            Debug.Print "LastDllError: "; Err.LastDllError
            Debug.Print "Source: "; Err.Source
            Debug.Print "Param1(strAppName): "; strAppName
            Debug.Print "Param2(strKeyName): "; strKeyName
            Debug.Print "Param3(strDefault): "; strDefault
            Debug.Print "Param4(strReturned): "; strReturned
            Debug.Print "Other(mstrFilePath): "; mstrFilePath
            Debug.Print "Other(mlngStringLength): "; mlngStringLength
            Debug.Print "Other(lngResult): "; lngResult
        #End If
        Err.Clear
        Exit Function
    End If
    
    On Error GoTo 0
    
    If lngResult = 0 Then
        strReturned = strDefault
        GetString = Len(strDefault)
        Exit Function
    End If
    
    Dim n&
    
    n = InStr(1, strData, vbNullChar) - 1
    If n > 0 Then
        strReturned = Left(strData, n)
        GetString = n
    Else
        strReturned = strDefault
        GetString = Len(strDefault)
    End If
    
End Function

Private Sub Class_Initialize()
    mlngStringLength = 1024
    mblnNoPath = True
End Sub
