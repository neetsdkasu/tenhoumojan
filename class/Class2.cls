VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**************************************************
'** ２者間データ
'**************************************************

Private mstrPlayerName()    As String

Private mlngPlayCount()       As Long

Private mlngScore()         As Long

Private mlngRankTotal()     As Long

Private mlngWonCount0()       As Long

Public Property Get PlayerName(p As Integer)
    PlayerName = mstrPlayerName(p)
End Property

Public Property Get PlayCount(n As Integer) As Long
    If n < 0 Then
        PlayCount = mlngPlayCount(0) + mlngPlayCount(1)
    Else
        PlayCount = mlngPlayCount(n)
    End If
End Property

Public Function GetString( _
                n As Integer, _
                Optional blnCompare As Boolean = False, _
                Optional cls1_0 As Class1, _
                Optional cls1_1 As Class1) As String
    Dim v
    Dim f1%, f2%, i%, sng1!
    Dim wc&(1), avr!(1), wrs$(1), avrs$(1), avss$(1)
    
    wc(0) = mlngWonCount0(n)
    wc(1) = mlngPlayCount(n) - wc(0)
    
    avr(0) = AverageRank(n, 0)
    avr(1) = AverageRank(n, 1)
    
    If blnCompare Then
        sng1 = avr(0) - cls1_0.AverageRank(n)
        avrs(0) = IIf(sng1 < -0.1!, "COLOR(blue):", _
                    IIf(sng1 > 0.1!, "COLOR(red):", ""))
        sng1 = avr(1) - cls1_1.AverageRank(n)
        avrs(1) = IIf(sng1 < -0.1!, "COLOR(blue):", _
                    IIf(sng1 > 0.1!, "COLOR(red):", ""))
        sng1 = AverageScore(n, 0) - cls1_0.AverageScore(n)
        avss(0) = IIf(sng1! > 5!, "COLOR(blue):", _
                    IIf(sng1 < -5!, "COLOR(red):", ""))
        sng1 = AverageScore(n, 1) - cls1_1.AverageScore(n)
        avss(1) = IIf(sng1! > 5!, "COLOR(blue):", _
                    IIf(sng1 < -5!, "COLOR(red):", ""))
        
    Else
        For i = 0 To 1
            If n = 0 Then
                avrs(i) = IIf(avr(i) < 2.3!, "COLOR(blue):", _
                                IIf(avr(i) > 2.7!, "COLOR(red):", ""))
            Else
                avrs(i) = IIf(avr(i) < 1.8!, "COLOR(blue):", _
                                IIf(avr(i) > 2.2!, "COLOR(red):", ""))
            End If
        Next i
    End If
    
    If wc(1) < wc(0) Then
        f1 = 1
        f2 = 0
    Else
        f1 = 0
        f2 = 1
    End If
    
    If wc(f1) < wc(f2) Then
        wrs(0) = "COLOR(red):"
        wrs(1) = "COLOR(blue):"
    Else
        wrs(0) = "COLOR(#0A0):"
        wrs(1) = "COLOR(#0A0):"
    End If
    
    v = Array( _
        "[[" & mstrPlayerName(f1) & "]]", _
        avrs(f1) & Format(avr(f1), "0.00"), _
        CStr(mlngScore(n, f1)), _
        avss(f1) & Format(AverageScore(n, f1), "0.00"), _
        wrs(0) & Format(WinnersRate(n, f1) * 100!, "00.0"), _
        IIf(mlngPlayCount(n) >= 100, "COLOR(#0A0):", "") _
                & CStr(mlngPlayCount(n)), _
        CStr(wc(f1)) & ":" & CStr(wc(f2)), _
        wrs(1) & Format(WinnersRate(n, f2) * 100!, "00.0"), _
        avss(f2) & Format(AverageScore(n, f2), "0.00"), _
        CStr(mlngScore(n, f2)), _
        avrs(f2) & Format(avr(f2), "0.00"), _
        "[[" & mstrPlayerName(f2) & "]]")
    GetString = "|" & Join(v, "|") & "|"
    
End Function

Public Property Get AverageScore(n As Integer, p As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        AverageScore = 0
    Else
        AverageScore = CSng(mlngScore(n, p)) / mlngPlayCount(n)
    End If
End Property

Public Property Get AverageRank(n As Integer, p As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        AverageRank = 0
    Else
        AverageRank = CSng(mlngRankTotal(n, p)) / CSng(mlngPlayCount(n))
    End If
End Property

Public Property Get WinnersRate(n As Integer, p As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        WinnersRate = 0
    Else
        If p = 0 Then
            WinnersRate = CSng(mlngWonCount0(n)) / CSng(mlngPlayCount(n))
        Else
            WinnersRate = CSng(mlngPlayCount(n) - mlngWonCount0(n)) _
                            / CSng(mlngPlayCount(n))
        End If
    End If
End Property

Public Function ExistName(strPlayer As String) As Boolean
    ExistName = (mstrPlayerName(0) = strPlayer) Or (mstrPlayerName(1) = strPlayer)
End Function

Public Sub SetPlayer(strPlayer0 As String, strPlayer1 As String)
    mstrPlayerName(0) = strPlayer0
    mstrPlayerName(1) = strPlayer1
End Sub

Public Sub AddData( _
            strRule As String, _
            intScore0 As Integer, intScore1 As Integer, _
            intRank0 As Integer, intRank1 As Integer)
    Dim n As Integer
    n = IIf(InStr(1, strRule, "三"), 1, 0)
    mlngPlayCount(n) = mlngPlayCount(n) + 1
    mlngScore(n, 0) = mlngScore(n, 0) + CLng(intScore0)
    mlngScore(n, 1) = mlngScore(n, 1) + CLng(intScore1)
    mlngRankTotal(n, 0) = mlngRankTotal(n, 0) + CLng(intRank0 + 1)
    mlngRankTotal(n, 1) = mlngRankTotal(n, 1) + CLng(intRank1 + 1)
    If intScore0 > intScore1 Then
        mlngWonCount0(n) = mlngWonCount0(n) + 1
    End If
End Sub

Private Sub Class_Initialize()
    ReDim mstrPlayerName(1)
    ReDim mlngScore(1, 1)
    ReDim mlngRankTotal(1, 1)
    ReDim mlngPlayCount(1)
    ReDim mlngWonCount0(1)
End Sub

Private Sub Class_Terminate()
    Erase mstrPlayerName
    Erase mlngScore
    Erase mlngRankTotal
End Sub
