VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**************************************************
'** データ集計・出力
'**************************************************

Public Event Progress(strProgress As String)
Public Event Message(strMessage As String)

Dim mfso1                   As Scripting.FileSystemObject
Dim mdicPlayers             As Scripting.Dictionary         'プレーヤーデータ

Dim mstrFolder              As String   '保存先フォルダパス
Dim mstrMemberListHeader    As String   'メンバーリストの項目
Dim mstrCount1              As String
Dim mstrCount2              As String

Dim mvarKeys                As Variant  'ソートされたプレーヤー名（キー）
Dim mlngKeysUpperNum        As Long     '全プレーヤー数-1

Dim mNowDate                As Date     '今日の日付（または今日の日付とするもの）

'****************************************************************
'
'****************************************************************
Public Sub OutputTimePlayCount()
    '▼エラートラップ
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If

    '▼必須変数定義
    Dim txs1    As Scripting.TextStream
    Dim strTitle$, strFilePath$
    
    '▼固有処理用変数定義
    Dim i&, j%, p&
    Dim tmp$
    Dim cls1    As Class1

    '▼ファイル生成
    strTitle = "PlayCountAtTime"      'ファイル名指定
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    '▼ファイル冒頭
    txs1.WriteLine "時間別対戦頻度"
    txs1.WriteBlankLines 1
    
    tmp = "||"
    For i = 0 To 23
        tmp = tmp & CStr(i) & "|"
    Next i
    
    txs1.WriteLine tmp & "対戦数|"

    For j = 0 To 9
        tmp = "|" & Mid("全体月曜火曜水曜木曜金曜土曜日曜平日土日", j * 2 + 1, 2) & "|"
        For i = 0 To 23
            If gintTimePlayCount(j, i) > 0 Then
                p = (CLng(gintTimePlayCount(j, i)) * 100&) _
                        \ CLng(gintTimePlayCount(j, 24))
                tmp = tmp & CStr(p)
            End If
            tmp = tmp & "|"
        Next i
        txs1.WriteLine tmp & CStr(gintTimePlayCount(j, 24)) & "|"
    Next j

    txs1.WriteBlankLines 2
    
    tmp = "|||"
    For i = 0 To 23
        tmp = tmp & CStr(i) & "|"
    Next i
    
    txs1.WriteLine tmp & "対戦数|"

    For i = 0 To mlngKeysUpperNum
        Set cls1 = mdicPlayers.Item(mvarKeys(i))
        If cls1.PlayCountLast3Month >= 15 Then
            tmp = "|[[" & cls1.PlayerName & "]]|平日|"
            For j = 0 To 23
                If cls1.PlayCountAtTime(0, j) > 0 Then
                    p = (100& * CLng(cls1.PlayCountAtTime(0, j))) _
                            \ CLng(cls1.PlayCountAtTime(0, 24))
                    tmp = tmp & CStr(p)
                End If
                tmp = tmp & "|"
            Next j
            txs1.WriteLine tmp & CStr(cls1.PlayCountAtTime(0, 24)) & "|"
            tmp = "|~|土日|"
            For j = 0 To 23
                If cls1.PlayCountAtTime(1, j) > 0 Then
                    p = (100& * CLng(cls1.PlayCountAtTime(1, j))) _
                            \ CLng(cls1.PlayCountAtTime(1, 24))
                    tmp = tmp & CStr(p)
                End If
                tmp = tmp & "|"
            Next j
            txs1.WriteLine tmp & CStr(cls1.PlayCountAtTime(1, 24)) & "|"
        End If
    Next i

    '▼ファイル閉じる
    txs1.Close
    Set txs1 = Nothing

End Sub


'****************************************************************
'
'****************************************************************
Public Sub OutputDanList(Optional lngMinPlayCount As Long = 50&)

    '▼エラートラップ
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If

    '▼必須変数定義
    Dim txs1    As Scripting.TextStream
    Dim strTitle$, strFilePath$
    
    '▼固有処理用変数定義
    Dim cls1    As Class1
    Dim i&
    Dim p0$, p1$, r0$, r1$, d0$, d1$

    '▼ファイル生成
    strTitle = "DanList"      'ファイル名指定
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    '▼ファイル冒頭
    txs1.WriteLine "段位リスト"
    txs1.WriteBlankLines 1

    txs1.WriteLine "|>|>|>|>|>|四麻|>|>|>|>|>|三麻|名前|"
    txs1.WriteLine _
        "|対戦数|平順|段位|pt|Rate|？|" _
        & "対戦数|平順|段位|pt|Rate|？|~|"

    '▼データの書き出し
    For i = 0& To mlngKeysUpperNum
        If mvarKeys(i) <> "NoName" Then
            Set cls1 = mdicPlayers.Item(mvarKeys(i))
            If (cls1.PlayCount(0) >= lngMinPlayCount) Or (cls1.PlayCount(1) >= lngMinPlayCount) Then
                p0 = "": p1 = "": r0 = "": r1 = "": d0 = "": d1 = ""
                If cls1.PlayCount(0) < lngMinPlayCount Then
                    p0 = "COLOR(#BBB):"
                Else
                    If cls1.Dan(0) > 11 Then
                        d0 = "COLOR(blue):"
                    End If
                    If cls1.Rating(0) >= 1800! Then
                        r0 = "COLOR(blue):"
                    End If
                End If
                If cls1.PlayCount(1) < lngMinPlayCount Then
                    p1 = "COLOR(#BBB):"
                Else
                    If cls1.Dan(1) > 11 Then
                        d1 = "COLOR(blue):"
                    End If
                    If cls1.Rating(1) >= 1800! Then
                        r1 = "COLOR(blue):"
                    End If
                End If
                
                txs1.WriteLine "|" _
                        & p0 & CStr(cls1.PlayCount(0)) & "|" _
                        & p0 & Format(cls1.AverageRank(0), "0.00") & "|" _
                        & p0 & d0 & gstrDanName(cls1.Dan(0)) & "|" _
                        & p0 & CStr(cls1.DanPt(0)) & "|" _
                        & p0 & r0 & CStr(CInt(Int(cls1.Rating(0)))) & "|" _
                        & p0 & CStr(cls1.Power(0)) & "|" _
                        & p1 & CStr(cls1.PlayCount(1)) & "|" _
                        & p1 & Format(cls1.AverageRank(1), "0.00") & "|" _
                        & p1 & d1 & gstrDanName(cls1.Dan(1)) & "|" _
                        & p1 & CStr(cls1.DanPt(1)) & "|" _
                        & p1 & r1 & CStr(CInt(Int(cls1.Rating(1)))) & "|" _
                        & p1 & CStr(cls1.Power(1)) & "|[[" _
                        & cls1.PlayerName & "]]|"
            End If
        End If
    Next i

    '▼ファイル閉じる
    txs1.Close
    Set txs1 = Nothing

End Sub

'****************************************************************
'
'****************************************************************
Public Sub OutputProfileList()
    
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
    Dim txs1    As Scripting.TextStream
    Dim cls1    As Class1
    Dim strTitle$, strFilePath$
    Dim i&, lngDate6MonthAgo&, lngNow&, lngDate3MonthAgo&
    Dim lngDate1YearAgo&

    lngNow = DateToLong(mNowDate)
    lngDate6MonthAgo = DateToLong(DateAdd("m", -6, mNowDate))
    lngDate3MonthAgo = DateToLong(DateAdd("m", -3, mNowDate))
    lngDate1YearAgo = DateToLong(DateAdd("m", -12, mNowDate))

    strTitle = "ProfileList"
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    txs1.WriteLine "３麻４麻合計30戦以上者リスト"
    txs1.WriteBlankLines 1
    txs1.WriteLine CStr(lngDate6MonthAgo) & " 〜 " & CStr(lngNow)
    txs1.WriteBlankLines 1
    Dim st$
    st = "|名前|初/参加/年|対戦/総数|最近/参加|" _
        & "段位|出身/地|現/住所|職業|喪雀/月間/上位/入賞|喪雀/大会/優勝|" _
        & "喪雀/大会/幹事|外部/大会/喪雀/代表|他/使用/ID|その他|"
    st = Replace(st, "/", "&br()")
    txs1.WriteLine st
    Dim c&

    For i = 0 To mlngKeysUpperNum
        Set cls1 = mdicPlayers.Item(mvarKeys(i))
        If cls1.PlayCount(-1) >= 30 Then
            If cls1.LastDate(-1) > lngDate1YearAgo Then
                If cls1.PlayerName <> "NoName" Then
                    c = c + 1
                    txs1.WriteLine "|[[" & cls1.PlayerName & "]]|" _
                        & CStr(cls1.FirstDate(-1) \ 10000&) & "|" _
                        & CStr(cls1.PlayCount(-1)) & "|" _
                        & IIf(cls1.LastDate(-1) > lngDate3MonthAgo, "◎", _
                            IIf(cls1.LastDate(-1) > lngDate6MonthAgo, "○", "")) _
                        & "|||||||||||"
                End If
            End If
        End If
    Next i
    
    txs1.WriteLine "Total " & CStr(c)

    txs1.Close
    
    Set txs1 = Nothing

End Sub

'****************************************************************
'
'****************************************************************
Public Sub OutputRuleCount(dicRuleCount As Scripting.Dictionary, _
                        Optional intFirstYear As Integer = 2007)
    Dim v, w, c&, i&, j&, Y%
    Dim txs1    As Scripting.TextStream
    Dim strTitle$, strFilePath$
    Dim dic1    As Scripting.Dictionary
    Dim str1    As String
    
    Y = CInt(Year(mNowDate))
    
    v = dicRuleCount.Keys()
    c = UBound(v)
    For i = 0 To c
        For j = 0 To c - 1
            If StrComp(v(j), v(j + 1), vbTextCompare) > 0 Then
                w = v(j)
                v(j) = v(j + 1)
                v(j + 1) = w
            End If
        Next j
    Next i
    
    strTitle = "RulePlayCount"
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    txs1.WriteLine "各ルール別対戦数"
    txs1.WriteBlankLines 1
    str1 = "|ルール|BGCOLOR(#FFD):Total|"
    For j = intFirstYear To Y
        str1 = str1 & CStr(j) & "|"
    Next j
    txs1.WriteLine str1
    
    For i = 0 To c
        Set dic1 = dicRuleCount.Item(v(i))
        str1 = "|" & v(i) & "|BGCOLOR(#FFD):" & dic1.Item("Total") & "|"
        For j = intFirstYear To Y
            If dic1.Exists(CStr(j)) Then
                str1 = str1 & dic1.Item(CStr(j)) & "|"
            Else
                str1 = str1 & "0|"
            End If
        Next j
        txs1.WriteLine str1
    Next i

    txs1.Close
    Set txs1 = Nothing

End Sub

'****************************************************************
'
'****************************************************************
'引数
' dic2Players   ２者同卓データ
' n             ３麻(1)か４麻(0)か
' lngPlayLine   集計作業簡略化のための最小対戦数の制限
' lngMemberLine 出力されるペア数
Public Sub Output2Players( _
            dic2Players As Scripting.Dictionary, _
            n As Integer, _
            Optional lngPlayLine As Long = 50, _
            Optional lngMemberLine As Long = 30)
    Dim v, i&, j&, v2
    Dim strTitle$
    Dim dic1 As Scripting.Dictionary
    Dim txs1 As Scripting.TextStream
    Dim cls2a As Class2
    Dim cls2b As Class2
    Dim cls1a As Class1
    Dim cls1b As Class1
    
    Set dic1 = New Scripting.Dictionary
    
    v = dic2Players.Keys()
    For i = 0 To UBound(v)
        Set cls2a = dic2Players.Item(v(i))
        If cls2a.PlayCount(n) >= lngPlayLine Then
            dic1.Add v(i), 0
        End If
    Next i
    
    v = dic1.Keys()
    
    For i = 0 To UBound(v)
        For j = 0 To UBound(v) - 1
            If StrComp(CStr(v(j)), CStr(v(j + 1))) > 0 Then
                v2 = v(j)
                v(j) = v(j + 1)
                v(j + 1) = v2
            End If
        Next j
    Next i
    
    For i = 0 To UBound(v)
        For j = 0 To UBound(v) - 1
            Set cls2a = dic2Players.Item(v(j))
            Set cls2b = dic2Players.Item(v(j + 1))
            If cls2a.PlayCount(n) < cls2b.PlayCount(n) Then
                v2 = v(j)
                v(j) = v(j + 1)
                v(j + 1) = v2
            End If
        Next j
    Next i
    
    strTitle = "About2Players" & CStr(4 - n) & "p"
    Set txs1 = GetTextStream(strTitle)
    
    txs1.WriteLine "２者間の関係 " & CStr(4 - n) & "p"
    txs1.WriteBlankLines 1
    txs1.WriteLine "|プレーヤー1|平均&br()順位1|通算&br()得点1|" _
        & "平均&br()得点1|勝率1&br()(%)|対戦数|勝数|" _
        & "勝率2&br()(%)|平均&br()得点2|通算&br()得点2|平均&br()順位2|" _
        & "プレーヤー2|"
    j = UBound(v)
    j = IIf(j > (lngMemberLine - 1), lngMemberLine - 1, j)
    For i = 0 To j
        Set cls2a = dic2Players.Item(v(i))
        Set cls1a = mdicPlayers.Item(cls2a.PlayerName(0))
        Set cls1b = mdicPlayers.Item(cls2a.PlayerName(1))
        txs1.WriteLine cls2a.GetString(n, True, cls1a, cls1b)
    Next i
    
    txs1.Close
    Set txs1 = Nothing
    
End Sub

'****************************************************************
'
'****************************************************************
Public Function Init( _
        dicPlayers As Scripting.Dictionary, _
        strFolderPath As String, _
        NowDate As Date) As Long
    
    Init = 0
    
    mNowDate = NowDate
    
    If Not (mfso1 Is Nothing) Then
        RaiseEvent Progress("Error")
        Init = -1
        Exit Function
    End If
    
    If (dicPlayers Is Nothing) Or (strFolderPath = "") Then
        RaiseEvent Progress("Error")
        Init = -2
        Exit Function
    End If
    
    If dicPlayers.Count = 0 Then
        RaiseEvent Progress("Error")
        Init = -3
        Exit Function
    End If
    
    Dim vTmp            '汎用変数(Variant)
    Dim i&, j&          'ループカウンタ(Long)
    
    Set mfso1 = New Scripting.FileSystemObject
    mstrFolder = mfso1.GetParentFolderName(strFolderPath)
    If mfso1.FolderExists(mstrFolder) = False Then
        Set mfso1 = Nothing
        RaiseEvent Progress("Error")
        Init = -4
        Exit Function
    End If
    
    Set mdicPlayers = dicPlayers
    
    'キーのソート
    mvarKeys = mdicPlayers.Keys()
    mlngKeysUpperNum = UBound(mvarKeys)
    For i = 0 To mlngKeysUpperNum
        For j = 0 To mlngKeysUpperNum - 1
            If StrComp(CStr(mvarKeys(j)), CStr(mvarKeys(j + 1))) > 0 Then
                vTmp = mvarKeys(j)
                mvarKeys(j) = mvarKeys(j + 1)
                mvarKeys(j + 1) = vTmp
            End If
        Next j
        RaiseEvent Progress(Str(i) & "/" & Str(mlngKeysUpperNum))
    Next i
    
    If mstrMemberListHeader = "" Then
    
        vTmp = Array( _
                "名前", _
                "区&br()分", _
                "初参加", _
                "最終参加", _
                "対戦&br()数", _
                "通算&br()得点", _
                "平均&br()得点", _
                "1&br()位", _
                "2&br()位", _
                "3&br()位", _
                "4&br()位", _
                "平均&br()順位", _
                "参加年&br()(対戦数)")
        
        mstrMemberListHeader = "|CENTER:" & Join(vTmp, "|CENTER:") & "|"
    
    End If
    
    mstrCount1 = ""
    mstrCount2 = ""
    
End Function

'****************************************************************
'
'****************************************************************
Public Sub Release()
    Set mdicPlayers = Nothing
    Set mfso1 = Nothing
    mvarKeys = Empty
    mstrCount1 = ""
    mstrCount2 = ""
End Sub

'****************************************************************
'
'****************************************************************
Private Function GetTextStream( _
                strTitle As String, _
                Optional strExtension As String = "txt") As Scripting.TextStream
    Dim strFilePath$
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder, strExtension)
    Set GetTextStream = mfso1.CreateTextFile(strFilePath)
End Function

'****************************************************************
'年度別参加者を出力する
'****************************************************************
Public Sub OutputMemberListAtYear( _
            Optional lngFirstYear As Long = 2007, _
            Optional lngPlayLine As Long = 20, _
            Optional lngMemberLine As Long = 200)
    '各年参戦者
    
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
'    Dim k%, y%, g%, gg%, ggg%, pp%  '汎用変数(Integer)
'    Dim d1&, d2&                    '汎用変数(Long)
'    Dim str1$                       '汎用変数(String)
    Dim i&, j&                      'ループカウンタ(Long)
    Dim lngNumberALL&
    Dim lngNumberAtYear&
    Dim lngNumberAtBlock&
    Dim intCountType%
    Dim lngThisYear&
    Dim lngCountStartYear&
    Dim lngCountEndYear&
    Dim strTitle$
    Dim strFilePath$
    Dim strText$
'    Dim strFile2$                   'ファイル名
    Dim txs1    As Scripting.TextStream
    Dim cls1    As Class1
    
    lngThisYear = CLng(Year(mNowDate))
'    lngCountStartYear = 20070101
'    lngCountEndYear = 20080101
    lngCountStartYear = lngFirstYear * 10000& + 101&
    lngCountEndYear = (lngFirstYear + 1) * 10000& + 101&
    
    For j = lngFirstYear To lngThisYear
        
        lngNumberAtYear = 0
        
        For intCountType = 0 To 1
            
            strTitle = "Member_" & Format(j) & "_" & CStr(lngPlayLine) & "戦" _
                                & IIf(intCountType = 0, "未満", "以上")
            strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
            Set txs1 = mfso1.CreateTextFile(strFilePath)
            
            txs1.WriteLine "メンバーリスト " & Str(j) & " " & CStr(lngPlayLine) _
                            & "戦" & IIf(intCountType = 0, "未満", "以上")
            txs1.WriteBlankLines 1
            txs1.WriteLine mstrMemberListHeader
            
            lngNumberAtBlock = 0
            
            For i = 0 To mlngKeysUpperNum
                
                Set cls1 = mdicPlayers.Item(mvarKeys(i))
                
                If (lngCountStartYear <= cls1.FirstDate(-1)) _
                            And (cls1.FirstDate(-1) < lngCountEndYear) Then
                    
                    If ((intCountType = 0) And (cls1.PlayCount(-1) < lngPlayLine)) _
                        Or ((intCountType = 1) _
                                And (cls1.PlayCount(-1) >= lngPlayLine)) Then
                        
                        '四麻
                        strText = cls1.GetString3(0)
                        If Len(strText) > 0 Then
                            txs1.WriteLine strText
                        End If
                        
                        '三麻
                        strText = cls1.GetString3(1)
                        If Len(strText) > 0 Then
                            txs1.WriteLine strText
                        End If
                        
                        lngNumberAtBlock = lngNumberAtBlock + 1
                        
                        If (lngNumberAtBlock Mod lngMemberLine) = 0 Then
                            
                            txs1.Close
                            Set txs1 = Nothing
                            
                            mstrCount1 = mstrCount1 & strTitle & vbTab _
                                            & Str(lngMemberLine) & vbNewLine

                            strTitle = "Member_" & Format(j) & "_" _
                                            & CStr(lngPlayLine) & "戦" _
                                            & IIf(intCountType = 0, "未満", "以上")
                            strTitle = strTitle _
                                & "[" & Format(lngNumberAtBlock \ lngMemberLine) _
                                & "]"
                            strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
                            Set txs1 = mfso1.CreateTextFile(strFilePath)
                            
                            txs1.WriteLine "メンバーリスト " & Str(j) & " " _
                                    & CStr(lngPlayLine) & "戦" _
                                            & IIf(intCountType = 0, "未満", "以上")
                            txs1.WriteBlankLines 1
                            txs1.WriteLine mstrMemberListHeader
                        
                        End If
                   
                   End If
                   
                End If
                
'                lblState.Caption = Str(j) & "/" & Str(k) & "/" _
'                                & Str(i) & "/" & Str(mlngKeysUpperNum)
'                DoEvents
                RaiseEvent Progress(Str(j) & "/" & Str(intCountType) & "/" _
                                & Str(i) & "/" & Str(mlngKeysUpperNum))
            
            Next i
            
            lngNumberAtYear = lngNumberAtYear + lngNumberAtBlock
            lngNumberALL = lngNumberALL + lngNumberAtBlock
            
            txs1.Close
            Set txs1 = Nothing
            
            mstrCount1 = mstrCount1 & strTitle & vbTab _
                            & Str(lngNumberAtBlock Mod lngMemberLine) & vbNewLine
            'txs1.WriteBlankLines 5
            
        Next intCountType
        
        mstrCount1 = mstrCount1 & Str(j) & vbTab _
                            & Str(lngNumberAtYear) & vbNewLine
        
        lngCountStartYear = lngCountStartYear + 10000&
        lngCountEndYear = lngCountEndYear + 10000&
        
    Next j
    
End Sub

'****************************************************************
'最近３ヶ月〜６ヶ月間の参加者を出力する
'****************************************************************
Public Sub OutputLast3to6MonthPlay()
    
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
    '3ヶ月参戦者
    Dim strText$
    Dim strTitle$
    Dim strFilePath$
    Dim lngPlayerCount&
    Dim lngDate3MonthAgo&
    Dim lngDate6MonthAgo&
    Dim j&
    Dim txs1    As Scripting.TextStream
    Dim cls1    As Class1
    
'    Dim dt
'    varDate3MonthAgo = DateAdd("m", -3, mNowDate)
'    d1 = CLng(Year(dt)) * 10000& + CLng(Month(dt)) * 100& + CLng(Day(dt))
    lngDate3MonthAgo = DateToLong(DateAdd("m", -3, mNowDate))
    lngDate6MonthAgo = DateToLong(DateAdd("m", -6, mNowDate))
    
    strTitle = "Member_Last3to6Month"
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    txs1.WriteLine "メンバーリスト 最近３ヶ月〜６ヶ月参加者"
    txs1.WriteBlankLines 1
    txs1.WriteLine CStr(lngDate6MonthAgo) & " 〜 " & CStr(lngDate3MonthAgo)
    txs1.WriteBlankLines 1
    txs1.WriteLine mstrMemberListHeader
    
    lngPlayerCount = 0
    For j = 0 To mlngKeysUpperNum
        Set cls1 = mdicPlayers.Item(mvarKeys(j))
        If cls1.LastDate(-1) < lngDate3MonthAgo Then
            If cls1.LastDate(-1) >= lngDate6MonthAgo Then
                strText = cls1.GetString3(0)
                If Len(strText) > 0 Then
                    txs1.WriteLine strText
                End If
                
                strText = cls1.GetString3(1)
                If Len(strText) > 0 Then
                    txs1.WriteLine strText
                End If
                
                lngPlayerCount = lngPlayerCount + 1
                RaiseEvent Progress(Str(lngPlayerCount) & "/" & Str(j) _
                                            & "/" & Str(mlngKeysUpperNum))
            End If
        End If
    Next j
    
    txs1.Close
    Set txs1 = Nothing
    
    mstrCount2 = mstrCount2 & "最近３ヶ月〜６ヶ月参加者 " & vbTab _
                            & Str(lngPlayerCount) & vbNewLine
    
End Sub

'****************************************************************
'最近３ヶ月参加者を出力する
'****************************************************************
Public Sub OutputLast3MonthPlay()
    
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
    '3ヶ月参戦者
    Dim strText$
    Dim strTitle$
    Dim strFilePath$
    Dim lngPlayerCount&
    Dim lngDate3MonthAgo&
    Dim j&
    Dim txs1    As Scripting.TextStream
    Dim cls1    As Class1
    
'    Dim dt
'    varDate3MonthAgo = DateAdd("m", -3, mNowDate)
'    d1 = CLng(Year(dt)) * 10000& + CLng(Month(dt)) * 100& + CLng(Day(dt))
    lngDate3MonthAgo = DateToLong(DateAdd("m", -3, mNowDate))
    
    strTitle = "Member_Last3Month"
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    txs1.WriteLine "メンバーリスト 最近３ヶ月参加者"
    txs1.WriteBlankLines 1
    txs1.WriteLine CStr(lngDate3MonthAgo) & " 〜 " & CStr(DateToLong(mNowDate))
    txs1.WriteBlankLines 1
    txs1.WriteLine mstrMemberListHeader
    
    lngPlayerCount = 0
    For j = 0 To mlngKeysUpperNum
        Set cls1 = mdicPlayers.Item(mvarKeys(j))
        If cls1.LastDate(-1) >= lngDate3MonthAgo Then
            
            strText = cls1.GetString3(0)
            If Len(strText) > 0 Then
                txs1.WriteLine strText
            End If
            
            strText = cls1.GetString3(1)
            If Len(strText) > 0 Then
                txs1.WriteLine strText
            End If
            
            lngPlayerCount = lngPlayerCount + 1
'            lblState.Caption = Str(g) & "/" & Str(j) & "/" & Str(vc)
'            DoEvents
            RaiseEvent Progress(Str(lngPlayerCount) & "/" & Str(j) _
                                        & "/" & Str(mlngKeysUpperNum))
        End If
    Next j
    
    txs1.Close
    Set txs1 = Nothing
    
    mstrCount2 = mstrCount2 & "最近３ヶ月参加者 " & vbTab _
                            & Str(lngPlayerCount) & vbNewLine
    
End Sub

'****************************************************************
'各種人数を出力する
'引数
' fso1          : ファイルシステムオブジェクトのインスタンス
' strFolder     : 保存先のフォルダパス
' ggg           : プレイヤーの全人数
' strCount      : 出力する人数データ
'****************************************************************
Public Sub OutputMembersCount()
    
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
'    If mstrCount1 = "" Then
'        'カウントしろ
'        Exit Sub
'    End If
'
'    If mstrCount2 = "" Then
'        'カウントしろ
'        Exit Sub
'    End If
    
    'メンバー数
    
    Dim strTitle$       'ファイルタイトル
    Dim strFilePath$    'ファイルパス
    Dim txs1 As Scripting.TextStream
    
    strTitle = "Member_Count"
    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
    Set txs1 = mfso1.CreateTextFile(strFilePath)
    
    txs1.WriteLine "メンバーリスト 人数" & vbTab & Str(mlngKeysUpperNum + 1&)
    txs1.WriteBlankLines 1
    txs1.WriteLine mstrCount1 & mstrCount2
    
    txs1.Close
    Set txs1 = Nothing
    
End Sub

'****************************************************************
'全員の名前一覧を出力する
'引数
' fso1          : ファイルシステムオブジェクトのインスタンス
' strFolder     : 保存先のフォルダパス
' dicPlayers    : プレイヤーの集計データ
' vc            : dicPlayersのKeyの上限値
' v             : 整列されたdicPlayersのKey
' ggg           : プレイヤーの全人数
'****************************************************************
Public Sub OutputAllMenberList()
    
    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
    '全員名簿
    
    Dim g%                  '汎用変数(Integer)
    Dim str1$               '汎用変数(String)
    Dim i&                  'ループカウンタ(Long)
    Dim strFile2            'ファイル名
    Dim strTitle$
    Dim strFilePath$
    Dim txs1        As Scripting.TextStream
    Dim cls1        As Class1
    
    strTitle = "Member_ALL_List"
'    strFilePath = GetOnlyName(mfso1, strTitle, mstrFolder)
'    Set txs1 = mfso1.CreateTextFile(strFilePath)
    Set txs1 = GetTextStream(strTitle)
    
    txs1.WriteLine "メンバー全員リスト" & vbTab & Str(mlngKeysUpperNum + 1&)
    txs1.WriteBlankLines 1
    txs1.Write "|"
    
    g = 0
    For i = 0 To mlngKeysUpperNum
        
        If CStr(mvarKeys(i)) <> "NoName" Then
            
            txs1.Write "[[" & CStr(mvarKeys(i)) & "]]|"
            
            g = g + 1
            
            If (g Mod 5) = 0 Then
                txs1.WriteBlankLines 1
                txs1.Write "|"
            End If
            
        End If
        
    Next i
    
    If (g Mod 5) > 0 Then
        txs1.WriteLine String(5 - (g Mod 5), "|")
    End If
    
    txs1.Close
    Set txs1 = Nothing
    
    Set cls1 = Nothing
    
End Sub
            


'****************************************************************
'新参数を数えて出力する
'引数
' fso1          : ファイルシステムオブジェクトのインスタンス
' strFolder     : 保存先のフォルダパス
' dicPlayers    : プレイヤーの集計データ
' vc            : dicPlayersのKeyの上限値
' v             : 整列されたdicPlayersのKey
'****************************************************************
Public Sub OutputNewFaceCount()

    If mfso1 Is Nothing Then
        RaiseEvent Progress("Error")
        Exit Sub
    End If
    
    '新参数
    
    Dim g%              '汎用変数(Integer)
    Dim s3$, str1$      '汎用変数(String)
    Dim s               '汎用変数(Variant)
    Dim i&, j&          'ループカウンタ(Long)
    Dim strFile2$       'ファイル名
    Dim v2              'Key整列用
    Dim dicNewFace      As Scripting.Dictionary
    Dim txs1            As Scripting.TextStream
    Dim cls1            As Class1
    
    Set dicNewFace = New Scripting.Dictionary
    
    For i = 0 To mlngKeysUpperNum
        
        Set cls1 = mdicPlayers.Item(mvarKeys(i))
        
        For j = -1 To 1
            
            '参戦年月ごと
            s3 = Choose(j + 2, "全", "四", "三") _
                            & Str(cls1.FirstDate(CInt(j)) \ 100&)
            
            If dicNewFace.Exists(s3) Then
                g = CInt(dicNewFace.Item(s3))
                g = g + 1
                dicNewFace.Remove s3
            Else
                g = 1
            End If
            
            dicNewFace.Add s3, g
        
            '参戦年ごと
            s3 = Choose(j + 2, "全", "四", "三") _
                            & Str(cls1.FirstDate(CInt(j)) \ 10000&)
            
            If dicNewFace.Exists(s3) Then
                g = CInt(dicNewFace.Item(s3))
                g = g + 1
                dicNewFace.Remove s3
            Else
                g = 1
            End If
            
            dicNewFace.Add s3, g
        
        Next j
        
        'lblState.Caption = Str(i) & "/" & Str(vc)
        RaiseEvent Progress(Str(i) & "/" & Str(mlngKeysUpperNum))
        
    Next i
    
    v2 = dicNewFace.Keys()
    
    'キーを整列
    For i = 0 To UBound(v2)
        For j = 0 To UBound(v2) - 1
            If StrComp(CStr(v2(j)), CStr(v2(j + 1))) > 0 Then
                s = v2(j)
                v2(j) = v2(j + 1)
                v2(j + 1) = s
            End If
        Next j
        'lblState.Caption = Str(i) & "/" & Str(UBound(v2))
        RaiseEvent Progress(Str(i) & "/" & Str(UBound(v2)))
    Next i
    
    '出力
    str1 = "NewFaceCount"
'    strFile2 = GetOnlyName(mfso1, str1, mstrFolder)
'    Set txs1 = mfso1.CreateTextFile(strFile2)
    Set txs1 = GetTextStream(str1)
    
    txs1.WriteLine "新参人数" & vbTab
    txs1.WriteBlankLines 1
    'txs1.Writeline ""
    For i = 0 To UBound(v2)
        txs1.WriteLine v2(i) & "  " & dicNewFace.Item(v2(i))
    Next i
    
    txs1.Close
    Set txs1 = Nothing
    
    dicNewFace.RemoveAll
    Set dicNewFace = Nothing
    
    Set cls1 = Nothing
    
End Sub

'****************************************************************
'100戦以上の四麻参加者のデータ集計タイプＡ
'引数
' fso1          : ファイルシステムオブジェクトのインスタンス
' strFolder     : 保存先のフォルダパス
' dicPlayers    : プレイヤーの集計データ
' vc            : dicPlayersのKeyの上限値
' v             : 整列されたdicPlayersのKey
'****************************************************************
Public Sub OutputDetailA4p(Optional lngMinPlayCount As Long = 100&)

    If mfso1 Is Nothing Then
        Exit Sub
    End If

    '100戦以上者詳細４麻のみ
    
    Dim str1$, str2$, str3      '汎用変数(String)
    Dim d1                      '汎用変数(Long)
    Dim i&                      'ループカウンタ(Long)
    Dim strFile2$               'ファイル名
    Dim txs1        As Scripting.TextStream
    Dim cls1        As Class1
    
    str1 = "DetailsA4p" & CStr(lngMinPlayCount) & "play"
'    strFile2 = GetOnlyName(mfso1, str1, mstrFolder)
'    Set txs1 = mfso1.CreateTextFile(strFile2)
    Set txs1 = GetTextStream(str1)
    
    txs1.WriteLine CStr(lngMinPlayCount) & "戦以上者詳細(四麻のみ)"
    txs1.WriteBlankLines 1
    
    str2 = "|名前|対戦&br()数|通算&br()得点|平均&br()得点" _
                & "|>|>|>|平均順位得点|>|>|>|順位率" _
                & "|連対&br()率|推定&br()飛び&br()率|平均&br()順位|"
    
    str3 = "|~|~|~|~|1位|2位|3位|4位|1位|2位|3位|4位|~|~|~|"
    
    txs1.WriteLine str2
    txs1.WriteLine str3

    d1 = 0
    
    For i = 0 To mlngKeysUpperNum
        
        Set cls1 = mdicPlayers.Item(mvarKeys(i))
        
        If cls1.PlayCount(0) >= lngMinPlayCount Then
            
            If CStr(mvarKeys(i)) <> "NoName" Then
               
               txs1.WriteLine cls1.GetDetailString4p(True)
               d1 = d1 + 1
               
            End If
            
        End If
    
    Next i
    
    txs1.WriteLine Str(d1) & "人"
    
    txs1.Close
    Set txs1 = Nothing
    
    Set cls1 = Nothing

End Sub

'****************************************************************
'50戦以上の３麻参加者のデータ集計タイプＡ
'引数
' fso1          : ファイルシステムオブジェクトのインスタンス
' strFolder     : 保存先のフォルダパス
' dicPlayers    : プレイヤーの集計データ
' vc            : dicPlayersのKeyの上限値
' v             : 整列されたdicPlayersのKey
'****************************************************************
Public Sub OutputDetailA3p(Optional lngMinPlayCount As Long = 50&)

    If mfso1 Is Nothing Then
        Exit Sub
    End If

    '50戦以上者詳細３麻のみ
    
    Dim str1$, str2$, str3$     '汎用変数(String)
    Dim d1&                     '汎用変数(Long)
    Dim i&                      'ループカウンタ(Long)
    Dim strFile2$               'ファイル名
    Dim txs1        As Scripting.TextStream
    Dim cls1        As Class1
    
    str1 = "DetailsA3p" & CStr(lngMinPlayCount) & "play"
'    strFile2 = GetOnlyName(mfso1, str1, mstrFolder)
'    Set txs1 = mfso1.CreateTextFile(strFile2)
    Set txs1 = GetTextStream(str1)
    
    txs1.WriteLine CStr(lngMinPlayCount) & "戦以上者詳細(３麻のみ)"
    txs1.WriteBlankLines 1
    
    str2 = "|名前|対戦&br()数|通算&br()得点|平均&br()得点" _
                & "|>|>|平均順位得点|>|>|順位率" _
                & "|推定&br()飛び&br()率|平均&br()順位|"
    
    str3 = "|~|~|~|~|1位|2位|3位|1位|2位|3位|~|~|"
    
    txs1.WriteLine str2
    txs1.WriteLine str3
    
    d1 = 0
    For i = 0 To mlngKeysUpperNum
        
        Set cls1 = mdicPlayers.Item(mvarKeys(i))
        
        If cls1.PlayCount(1) >= lngMinPlayCount Then
            
            If CStr(mvarKeys(i)) <> "NoName" Then
               
               txs1.WriteLine cls1.GetDetailString3p(True)
               d1 = d1 + 1
               
            End If
            
        End If
        
    Next i
    
    txs1.WriteLine Str(d1) & "人"
    
    txs1.Close
    Set txs1 = Nothing
    
    Set cls1 = Nothing
    
End Sub




Private Sub Class_Terminate()
    Call Me.Release
End Sub
