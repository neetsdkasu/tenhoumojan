VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**************************************************
'** 個人データ
'**************************************************

Private mstrName As String

Private mlngFirstDate() As Long     '初参戦日 8桁の数字 2010/09/12 → 20100912
' 添え字1 0-四麻　1-三麻

Private mlngLastDate() As Long      '最終対戦日
' 添え字1 0-四麻　1-三麻

Private mlngPlayCount() As Long     '対戦数
' 添え字1 0-四麻　1-三麻

Private mlngTotalScore() As Long    'トータル点数
' 添え字1 0-四麻　1-三麻

Private mlngRankCount() As Long     '順位数
' 添え字1 0-四麻　1-三麻
' 添え字2 順位-1

Private mlngRankScore() As Long     '順位ごとのトータル点数
' 添え字1 0-四麻　1-三麻
' 添え字2 順位-1

Private mlngHakoten() As Long
Private mlngUma() As Long

Private mlngDanPoint() As Long      'ポイント段位値
Private msngRating()    As Single   'Rating
Private mlngPower()     As Long

Private mintTimePlay()  As Integer
' 添え字1 0-平日 1-土日
' 添え字2 0-23 時間
Private mintPlay3Month  As Integer

'Private mdicPlayers() As Scripting.Dictionary  '対戦相手の統計
' 添え字1 0-四麻　1-三麻
' Key 対戦者名
' Item 対戦数
'対戦相手のデータだったが多いのでカット

Private mdicPlayAtYear() As Scripting.Dictionary    '年ごとの対戦数
' 添え字 0-四麻 1-三麻
' Key 年
' Item 対戦数

'**************************************************
'初対戦年月日をLong値で返す
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻のうち過去のほう
Public Property Get FirstDate(n As Integer) As Long
    If n < 0 Then
        FirstDate = mlngFirstDate(IIf(mlngFirstDate(0) < mlngFirstDate(1), 0, 1))
    Else
        FirstDate = mlngFirstDate(n)
    End If
End Property

'**************************************************
'最近の対戦年月日をLong値で返す
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻のうち最近のほう
Public Property Get LastDate(n As Integer) As Long
    If n < 0 Then
        LastDate = mlngLastDate(IIf(mlngLastDate(0) > mlngLastDate(1), 0, 1))
    Else
        LastDate = mlngLastDate(n)
    End If
End Property

'**************************************************
'総対戦数をLong値で返す
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻の合算
Public Property Get PlayCount(n As Integer) As Long
    If n < 0 Then
        PlayCount = mlngPlayCount(0) + mlngPlayCount(1)
    Else
        PlayCount = mlngPlayCount(n)
    End If
End Property

'@@@@@@@@@ 不使用 @@@@@@@@@
'データを列挙して返す
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻の合算
Public Function GetString(n As Integer) As String
    If mlngPlayCount(n) = 0 Then
        GetString = ""
        Exit Function
    End If
    
    Dim v, v2, i, s, j
    v = Array( _
            mstrName, _
            mlngFirstDate(n), _
            mlngLastDate(n), _
            mlngPlayCount(n), _
            mlngTotalScore(n), _
            Format(AverageScore(n), "0.00"), _
            mlngRankCount(n, 0), _
            mlngRankCount(n, 1), _
            mlngRankCount(n, 2), _
            mlngRankCount(n, 3), _
            Format(AverageRank(n), "0.00"))
    v2 = mdicPlayAtYear(n).Keys()
    For i = 0 To UBound(v2)
        For j = 0 To UBound(v2) - 1
            If CInt(v2(j)) > CInt(v2(j + 1)) Then
                s = v2(j)
                v2(j) = v2(j + 1)
                v2(j + 1) = s
            End If
        Next j
    Next i
    
    For i = 0 To UBound(v2)
        v2(i) = v2(i) & "(" & Trim(mdicPlayAtYear(n).Item(v2(i))) & ")"
    Next i
    
    GetString = Join(v, " ") & " " & Join(v2, ",")
    
End Function

'@@@@@@@ 不使用 @@@@@@@@@@
'HTML書式でデータを返す
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻の合算
Public Function GetString2(n As Integer) As String

    Dim dt, d1&, d2&
    dt = DateAdd("m", -3, Now)
    d1 = 20070622
    d2 = CLng(Year(dt)) * 10000& + CLng(Month(dt)) * 100& + CLng(Day(dt))
    
    Dim sName$
    
    If n = 0 Then
        If mstrName = "NoName" Then
            sName = mstrName
        Else
            sName = "[[" & mstrName & "]]"
        End If
    Else
        sName = "~"
    End If

    Dim v, v2, i, s, j, av!, pc&
    If mlngPlayCount(n) > 0 Then
        av = AverageRank(n)
        v = Array( _
                sName, _
                IIf(n = 0, "４", "３") & "麻", _
                IIf(mlngFirstDate(n) < d1, "COLOR(red):", "") _
                        & Format(mlngFirstDate(n)), _
                IIf(mlngLastDate(n) >= d2, "COLOR(blue):", "") _
                        & Format(mlngLastDate(n)), _
                Format(mlngPlayCount(n)), _
                Format(mlngTotalScore(n)), _
                Format(AverageScore(n), "0.00"), _
                Format(mlngRankCount(n, 0)), _
                Format(mlngRankCount(n, 1)), _
                Format(mlngRankCount(n, 2)), _
                Format(mlngRankCount(n, 3)), _
                IIf(mlngPlayCount(n) > 100&, _
                                IIf(av < 2.3, "COLOR(blue):", "") _
                                    & IIf(av > 2.6, "COLOR(red):", ""), "") _
                        & Format(av, "0.00"))
        v2 = mdicPlayAtYear(n).Keys()
        For i = 0 To UBound(v2)
            For j = 0 To UBound(v2) - 1
                If CInt(v2(j)) > CInt(v2(j + 1)) Then
                    s = v2(j)
                    v2(j) = v2(j + 1)
                    v2(j + 1) = s
                End If
            Next j
        Next i
        
        For i = 0 To UBound(v2)
            v2(i) = v2(i) & "(" & Trim(mdicPlayAtYear(n).Item(v2(i))) & ")"
        Next i
        
        GetString2 = "|" & Join(v, "|") & "|" & Join(v2, "&br()") & "|"
    Else
        v = Array( _
                sName, _
                IIf(n = 0, "４", "３") & "麻", _
                "-", _
                "-", _
                mlngPlayCount(n), _
                "-", _
                "-", _
                "-", _
                "-", _
                "-", _
                "-", _
                "-")
            
        GetString2 = "|" & Join(v, "|") & "|-|"
    End If
End Function

'**************************************************
'atwiki形式でデータを返す
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻の合算
Public Function GetString3(n As Integer) As String
    If mlngPlayCount(n) = 0 Then
        GetString3 = ""
        Exit Function
    End If

    Dim dt, d1&, d2&, d3&
    d1 = 20070622
    'dt = DateAdd("m", -3, Now)
    'd2 = CLng(Year(dt)) * 10000& + CLng(Month(dt)) * 100& + CLng(Day(dt))
    d2 = DateToLong(DateAdd("m", -3, Now))
    'dt = DateAdd("m", -6, Now)
    'd3 = CLng(Year(dt)) * 10000& + CLng(Month(dt)) * 100& + CLng(Day(dt))
    d3 = DateToLong(DateAdd("m", -6, Now))
    
    Dim sName$
    
    If n = 0 Then
        If mstrName = "NoName" Then
            sName = mstrName
        Else
            sName = "[[" & mstrName & "]]"
        End If
    Else
        If mlngPlayCount(0) > 0 Then
            sName = "~"
        Else
            If mstrName = "NoName" Then
                sName = mstrName
            Else
                sName = "[[" & mstrName & "]]"
            End If
        End If
    End If

    Dim v, v2, i, s, j, ar!, ars$
        ar = AverageRank(n)
        ars = ""
        If n = 0 Then
            If mlngPlayCount(0) > 100& Then
                ars = IIf(ar < 2.3!, "COLOR(blue):", _
                        IIf(ar > 2.7!, "COLOR(red):", ""))
            End If
        ElseIf n = 1 Then
            If mlngPlayCount(1) > 50& Then
                ars = IIf(ar < 1.8!, "COLOR(blue):", _
                        IIf(ar > 2.2!, "COLOR(red):", ""))
            End If
        End If
        
        ars = ars & Format(ar, "0.00")
        v = Array( _
                sName, _
                IIf(n = 0, "４", "３") & "麻", _
                IIf(mlngFirstDate(n) < d1, "COLOR(red):", "") _
                        & Format(mlngFirstDate(n)), _
                IIf(mlngLastDate(n) >= d2, "COLOR(blue):", _
                IIf(mlngLastDate(n) >= d3, "COLOR(#0A0):", "")) _
                        & Format(mlngLastDate(n)), _
                Format(mlngPlayCount(n)), _
                Format(mlngTotalScore(n)), _
                Format(AverageScore(n), "0.00"), _
                Format(mlngRankCount(n, 0)), _
                Format(mlngRankCount(n, 1)), _
                Format(mlngRankCount(n, 2)), _
                IIf(n = 0, Format(mlngRankCount(n, 3)), "-"), _
                ars)
        v2 = mdicPlayAtYear(n).Keys()
        For i = 0 To UBound(v2)
            For j = 0 To UBound(v2) - 1
                If CInt(v2(j)) > CInt(v2(j + 1)) Then
                    s = v2(j)
                    v2(j) = v2(j + 1)
                    v2(j + 1) = s
                End If
            Next j
        Next i
        
        Dim y$
        y = CStr(Year(Now))
        
        For i = 0 To UBound(v2)
            
            v2(i) = IIf(y = CStr(v2(i)), "&font(blue){", "") _
                    & v2(i) & "(" & Trim(mdicPlayAtYear(n).Item(v2(i))) & ")" _
                    & IIf(y = CStr(v2(i)), "}", "")
            
        Next i
        
        GetString3 = "|" & Join(v, "|") & "|" & Join(v2, "&br()") & "|"

End Function

'**************************************************
'
Public Function GetDetailString4p(Optional bColor As Boolean = False) As String
    Dim v, rr$(3), ar!, i%, ars$, ts$, avsc!, avscs$, ps$
    Dim sName$, rt!, rts$, dr!, drs$
    
    sName = mstrName
    If sName <> "NoName" Then
        sName = "[[" & sName & "]]"
    End If
    
    ps = CStr(mlngPlayCount(0))
    ts = CStr(mlngTotalScore(0))
    For i = 0 To 3
        rr(i) = Format(RankRate(0, i), ".000")
    Next i
    ar = AverageRank(0)
    avsc = AverageScore(0)
    rt = Rentai()
    dr = DeadRate(0)
    ars = ""
    avscs = ""
    rts = ""
    drs = ""
    If bColor Then
        If mlngPlayCount(0) >= 1000& Then
            ps = "COLOR(#0A0):" & ps
        End If
        If mlngTotalScore(0) >= 1000& Then
            ts = "COLOR(blue):" & ts
        ElseIf mlngTotalScore(0) <= -1000& Then
            ts = "COLOR(red):" & ts
        End If
        If avsc > 5! Then
            avscs = "COLOR(blue):"
        ElseIf avsc < -5! Then
            avscs = "COLOR(red):"
        End If
        If dr > 0.1! Then
            drs = "COLOR(red):"
        ElseIf dr < 0.05! Then
            drs = "COLOR(#0A0):"
        End If
        
        If ar < 2.3! Then
            ars = "COLOR(blue):"
        ElseIf ar > 2.7! Then
            ars = "COLOR(red):"
        End If
        If RankRate(0, 0) > 0.3! Then
            rr(0) = "COLOR(blue):" & rr(0)
        ElseIf RankRate(0, 0) < 0.2! Then
            rr(0) = "COLOR(red):" & rr(0)
        End If
        For i = 1 To 2
            If RankRate(0, i) > 0.3! Then
                rr(i) = "COLOR(#0A0):" & rr(i)
            End If
        Next i
        If RankRate(0, 3) > 0.3! Then
            rr(3) = "COLOR(red):" & rr(3)
        ElseIf RankRate(0, 3) < 0.2! Then
            rr(3) = "COLOR(blue):" & rr(3)
        End If
        If rt > 0.55! Then
            rts = "COLOR(blue):"
        ElseIf rt > 0.5! Then
            rts = "COLOR(#0A0):"
        ElseIf rt < 0.4! Then
            rts = "COLOR(red):"
        End If

    End If
    ars = ars & Format(ar, "0.00")
    avscs = avscs & Format(avsc, "0.00")
    rts = rts & Format(rt, ".000")
    drs = drs & Format(dr, ".000")
    v = Array( _
            sName, _
            ps, ts, avscs, _
            Format(AverageRankScore(0, 0), "0.0"), _
            Format(AverageRankScore(0, 1), "0.0"), _
            Format(AverageRankScore(0, 2), "0.0"), _
            Format(AverageRankScore(0, 3), "0.0"), _
            rr(0), rr(1), rr(2), rr(3), rts, _
            drs, ars)
    GetDetailString4p = "|" & Join(v, "|") & "|"
    
End Function


'**************************************************
'
Public Function GetDetailString3p(Optional bColor As Boolean = False) As String
    Dim v, rr$(2), ar!, i%, ars$, ts$, avsc!, avscs$, ps$
    Dim sName$, dr!, drs$
    
    sName = mstrName
    If sName <> "NoName" Then
        sName = "[[" & sName & "]]"
    End If
    
    ps = CStr(mlngPlayCount(1))
    ts = CStr(mlngTotalScore(1))
    For i = 0 To 2
        rr(i) = Format(RankRate(1, i), ".000")
    Next i
    ar = AverageRank(1)
    avsc = AverageScore(1)
    dr = DeadRate(1)
    ars = ""
    avscs = ""
    drs = ""
    If bColor Then
        If mlngPlayCount(1) >= 100& Then
            ps = "COLOR(#0A0):" & ps
        End If
        If mlngTotalScore(1) >= 500& Then
            ts = "COLOR(blue):" & ts
        ElseIf mlngTotalScore(1) <= -500& Then
            ts = "COLOR(red):" & ts
        End If
        If avsc > 5! Then
            avscs = "COLOR(blue):"
        ElseIf avsc < -5! Then
            avscs = "COLOR(red):"
        End If
        If dr > 0.1! Then
            drs = "COLOR(red):"
        ElseIf dr < 0.05! Then
            drs = "COLOR(#0A0):"
        End If

        If ar < 1.8! Then
            ars = "COLOR(blue):"
        ElseIf ar > 2.2! Then
            ars = "COLOR(red):"
        End If
        If RankRate(1, 0) > 0.4! Then
            rr(0) = "COLOR(blue):" & rr(0)
        ElseIf RankRate(1, 0) < 0.27! Then
            rr(0) = "COLOR(red):" & rr(0)
        End If
        If RankRate(1, 1) > 0.4! Then
            rr(1) = "COLOR(#0A0):" & rr(1)
        End If
        If RankRate(1, 2) > 0.4! Then
            rr(2) = "COLOR(red):" & rr(2)
        ElseIf RankRate(1, 2) < 0.27! Then
            rr(2) = "COLOR(blue):" & rr(2)
        End If
        
    End If
    ars = ars & Format(ar, "0.00")
    avscs = avscs & Format(avsc, "0.00")
    drs = drs & Format(dr, ".000")
    v = Array( _
            sName, _
            ps, ts, avscs, _
            Format(AverageRankScore(1, 0), "0.0"), _
            Format(AverageRankScore(1, 1), "0.0"), _
            Format(AverageRankScore(1, 2), "0.0"), _
            rr(0), rr(1), rr(2), _
            drs, ars)
    GetDetailString3p = "|" & Join(v, "|") & "|"
    
End Function

'**************************************************
'平均順位を返す
'引数 n  (0)四麻 (1)三麻
Public Property Get AverageRank(n As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        AverageRank = 0
    Else
        AverageRank = CSng(mlngRankCount(n, 0) _
                            + mlngRankCount(n, 1) * 2 _
                            + mlngRankCount(n, 2) * 3 _
                            + mlngRankCount(n, 3) * 4) _
                    / CSng(mlngPlayCount(n))
    End If
End Property

'**************************************************
'平均得点を返す
'引数 n  (0)四麻 (1)三麻
Public Property Get AverageScore(n As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        AverageScore = 0
    Else
        AverageScore = CSng(mlngTotalScore(n)) / CSng(mlngPlayCount(n))
    End If
End Property

'**************************************************
'順位率を返す
'引数 n  (0)四麻 (1)三麻
'     r  順位 四麻なら0-3 三麻なら0-2
Public Property Get RankRate(n As Integer, r As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        RankRate = 0
    Else
        RankRate = CSng(mlngRankCount(n, r)) / CSng(mlngPlayCount(n))
    End If
End Property

'**************************************************
'平均順位得点
'引数 n  (0)四麻 (1)三麻
'     r  順位 四麻なら0-3 三麻なら0-2
Public Property Get AverageRankScore(n As Integer, r As Integer) As Single
    If mlngRankCount(n, r) = 0 Then
        AverageRankScore = 0
    Else
        Dim p&
        p = mlngRankScore(n, r)
        If n = 0 Then
            p = p _
                + mlngUma(0) * 10& _
                + mlngUma(1) * 5& _
                - mlngUma(2) * 5& _
                - mlngUma(3) * 10&
        End If
        AverageRankScore = CSng(p) / CSng(mlngRankCount(n, r))
    End If
End Property

'**************************************************
'連対率(四麻)
Public Property Get Rentai() As Single
    If mlngPlayCount(0) = 0 Then
        Rentai = 0
    Else
        Rentai = CSng(mlngRankCount(0, 0) + mlngRankCount(0, 1)) / CSng(mlngPlayCount(0))
    End If
End Property

'**************************************************
'飛び率
Public Property Get DeadRate(n As Integer) As Single
    If mlngPlayCount(n) = 0 Then
        DeadRate = 0
    Else
        DeadRate = CSng(mlngHakoten(n)) / CSng(mlngPlayCount(n))
    End If
End Property

'**************************************************
Public Sub AddPower(n As Integer, v As Long)
    mlngPower(n) = mlngPower(n) + v
End Sub

'**************************************************
'プレーヤー名を設定する
'引数 strName プレーヤー名
Public Sub SetName(strName As String)
    mstrName = strName
End Sub

'**************************************************
'情報を追加する
'引数
' lngDate   対戦日 (Long形式 1995/3/4なら 19950304 )
' strTime   対戦開始時間 ( hh:mm の形式)
' intRank   順位
' intScore  得点
' strRule   対戦のルール (四般南喰− や 三般東喰赤 など）
' strMember このプレーヤー含む対戦者名のリスト（順位順）
' intScores 各プレーヤーの得点
Public Sub AddData( _
                lngDate As Long, _
                strTime As String, _
                intRank As Integer, _
                intScore As Integer, _
                strRule As String, _
                blnJoTaku As Boolean, _
                sngAvgRating As Single, _
                intWeek As Integer)
'                strMember() As String, _
'                intScores() As Integer)

    Dim n%, t%, h%, w%
    
    If lngDate >= glngDate3MonthAgo Then
        h = Hour(strTime)
        w = IIf(intWeek < 6, 0, 1)
        mintTimePlay(w, h) = mintTimePlay(w, h) + 1
        mintTimePlay(w, 24) = mintTimePlay(w, 24) + 1
        mintPlay3Month = mintPlay3Month + 1
    End If
    
    '▼３麻(n=1)か４麻(n=0)か
    n = IIf(InStr(1, strRule, "四") > 0, 0, 1)

    '▼東風(t=1)か東南(t=0)か
    t = IIf(InStr(1, strRule, "南") > 0, 0, 1)

    '▼対戦数更新
    mlngPlayCount(n) = mlngPlayCount(n) + 1

    '▼初回対戦日更新
    If lngDate < mlngFirstDate(n) Then
        mlngFirstDate(n) = lngDate
    End If
    
    '▼最終対戦日更新
    If lngDate > mlngLastDate(n) Then
        mlngLastDate(n) = lngDate
    End If
    
    '▼年別対戦数更新
    Dim y$, p%
    y = CInt(Left(Format(lngDate), 4))
    If mdicPlayAtYear(n).Exists(y) Then
        p = CInt(mdicPlayAtYear(n).Item(y))
        mdicPlayAtYear(n).Item(y) = Str(p + 1)
    Else
        mdicPlayAtYear(n).Add y, "1"
    End If
    
    '▼ウマ考慮の飛び数更新
    If n = 0 Then
        If (lngDate < 20090801) And (t = 1) Then
            '5-10
            If (intRank = 1) And (intScore <= -26) Then
                mlngHakoten(0) = mlngHakoten(0) + 1
            ElseIf (intRank = 2) And (intScore <= -36) Then
                mlngHakoten(0) = mlngHakoten(0) + 1
            ElseIf (intRank = 3) And (intScore <= -41) Then
                mlngHakoten(0) = mlngHakoten(0) + 1
            End If
            mlngUma(intRank) = mlngUma(intRank) + 1
        Else
            '10-20
            If (intRank = 1) And (intScore <= -21) Then
                mlngHakoten(0) = mlngHakoten(0) + 1
            ElseIf (intRank = 2) And (intScore <= -41) Then
                mlngHakoten(0) = mlngHakoten(0) + 1
            ElseIf (intRank = 3) And (intScore <= -51) Then
                mlngHakoten(0) = mlngHakoten(0) + 1
            End If
        End If
    Else
        '+20-20
        If (intRank = 1) And (intScore <= -41) Then
                mlngHakoten(1) = mlngHakoten(1) + 1
        ElseIf (intRank = 2) And (intScore <= -61) Then
                 mlngHakoten(1) = mlngHakoten(1) + 1
        End If
    End If
    
    '▼通算得点更新
    mlngTotalScore(n) = mlngTotalScore(n) + CLng(intScore)

    '▼順位別得点更新
    mlngRankScore(n, intRank) = mlngRankScore(n, intRank) + CLng(intScore)
    
    '▼順位数更新
    mlngRankCount(n, intRank) = mlngRankCount(n, intRank) + 1
    
    '▼ポイント段位値更新
    If mstrName <> "NoName" Then
        mlngDanPoint(n) = ChangeDanPoint(mlngDanPoint(n), intRank, t = 0, n = 1, blnJoTaku)
        
        msngRating(n) = CalcRating(msngRating(n), mlngPlayCount(n), _
                                        intRank, sngAvgRating, n = 1)
    End If
    
    '↓さすがにデータ量多いから省く（同卓相手との関係）
'    Dim i%, q&
'    Dim dic As Scripting.Dictionary
'    For i = 0 To UBound(strMember)
'        If i <> intRank Then
'            If mdicPlayers(n).Exists(strMember(i)) Then
'                'p = CInt(mdicPlayers(n).Item(strMember(i)))
'                'mdicPlayers(n).Item(strMember(i)) = Str(p + 1)
'                Set dic = mdicPlayers(n).Item(strMember(i))
'                p = CInt(dic.Item("対戦数"))
'                dic.Item("対戦数") = Str(p + 1)
'                q = CLng(dic.Item("得点"))
'                dic.Item("得点") = Str(q + CLng(intScores(i)))
'                q = CLng(dic.Item("差分点"))
'                dic.Item("差分点") = Str(q + CLng(intScore - intScores(i)))
'                p = CInt(dic.Item("勝利数"))
'                dic.Item("勝利数") = Str(p + IIf(intRank > i, 1, 0))
'            Else
'                Set dic = New Scripting.Dictionary
'                dic.Add "対戦数", "1"
'                dic.Add "得点", Str(intScores(i))
'                dic.Add "差分点", Str(intScore - intScores(i))
'                dic.Add "勝利数", IIf(intRank > i, "1", "0")
'                mdicPlayers(n).Add strMember(i), dic
'            End If
'        End If
'    Next i
'    Set dic = Nothing
    
End Sub

'**************************************************
Public Property Get PlayCountLast3Month() As Integer
    PlayCountLast3Month = mintPlay3Month
End Property

'**************************************************
' 引数
'  intWeek 0-平日 1-土日
'  intHour 0-23 時間
Public Property Get PlayCountAtTime(intWeek As Integer, intHour As Integer) As Integer
    PlayCountAtTime = mintTimePlay(intWeek, intHour)
End Property

'**************************************************
'引数 n  (0)四麻 (1)三麻
Public Property Get Power(n As Integer) As Long
    Power = mlngPower(n)
End Property

'**************************************************
'引数 n  (0)四麻 (1)三麻
Public Property Get Rating(n As Integer) As Single
    Rating = msngRating(n)
End Property

'**************************************************
'段位値
'引数 n  (0)四麻 (1)三麻
Public Property Get Dan(n As Integer) As Long
    Dan = mlngDanPoint(n) Mod 100&
End Property

'**************************************************
'（段位制の）ポイント値
'引数 n  (0)四麻 (1)三麻
Public Property Get DanPt(n As Integer) As Long
    DanPt = (mlngDanPoint(n) - Me.Dan(n)) \ 100&
End Property

'**************************************************
'プレーヤー名
Public Property Get PlayerName() As String
    PlayerName = mstrName
End Property

'**************************************************
'通算得点
'引数 n  (0)四麻 (1)三麻 (負数)四麻と三麻の合計
Public Property Get TotalScore(n As Integer) As Long
    If n < 0 Then
        TotalScore = mlngTotalScore(0) + mlngTotalScore(1)
    Else
        TotalScore = mlngTotalScore(n)
    End If
End Property

'**************************************************
'順位数
'引数 n  (0)四麻 (1)三麻
'     r  順位 四麻なら0-3 三麻なら0-2
Public Property Get RankCount(n As Integer, r As Integer) As Long
    RankCount = mlngRankCount(n, r)
End Property

'**************************************************
'通算順位得点
'引数 n  (0)四麻 (1)三麻
'     r  順位 四麻なら0-3 三麻なら0-2
Public Property Get TotalRankScore(n As Integer, r As Integer) As Long
    TotalRankScore = mlngRankScore(n, r)
End Property

'**************************************************
'年ごとの対戦数
'引数 n  (0)四麻 (1)三麻
'引数 y  取得したい年を西暦4桁の数字で指定
Public Property Get PlayCountAtYear(n As Integer, y As Integer) As Long
    Dim yy$
    yy = CStr(y)
    If mdicPlayAtYear(n).Exists(yy) Then
        PlayCountAtYear = CLng(mdicPlayAtYear(n).Item(yy))
    Else
        PlayCountAtYear = 0
    End If
End Property

'/////////////////////////////////////////////////
'初期化
'
Private Sub Class_Initialize()
    ReDim mlngFirstDate(1)
    ReDim mlngLastDate(1)
    ReDim mlngTotalScore(1)
    ReDim mlngPlayCount(1)
'    ReDim mdicPlayers(1)
    ReDim mdicPlayAtYear(1)
    ReDim mlngRankCount(1, 3)
    ReDim mlngRankScore(1, 3)
    ReDim mlngHakoten(1)
    ReDim mlngUma(3)
    ReDim mlngDanPoint(1)
    ReDim msngRating(1)
    ReDim mlngPower(1)
    ReDim mintTimePlay(1, 24)
    Dim i%
    For i = 0 To 1
        mlngPower(i) = 10000&
        msngRating(i) = 1500!
        mlngFirstDate(i) = 99999999
'        Set mdicPlayers(i) = New Scripting.Dictionary
        Set mdicPlayAtYear(i) = New Scripting.Dictionary
    Next i
End Sub

'/////////////////////////////////////////////////
'破棄
'
Private Sub Class_Terminate()
    Dim i%
    For i = 0 To 1
'        mdicPlayers(i).RemoveAll
        mdicPlayAtYear(i).RemoveAll
    Next i
    Erase mlngFirstDate
    Erase mlngLastDate
    Erase mlngTotalScore
    Erase mlngPlayCount
    Erase mlngRankCount
    Erase mlngRankScore
    Erase mlngHakoten
    Erase mlngUma
    Erase mlngDanPoint
    Erase msngRating
'    Erase mdicPlayers
    Erase mdicPlayAtYear
End Sub
