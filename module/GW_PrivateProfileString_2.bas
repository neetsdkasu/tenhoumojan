Attribute VB_Name = "Win32API_GW_PPS"
Option Explicit


Private Declare Function GetPrivateProfileString _
    Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpDefault As String, _
        ByVal lpReturnedString As String, _
        ByVal nSize As Long, _
        ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString _
    Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpString As Any, _
        ByVal lpFileName As String) As Long


Private Const STRING_LENGTH As Integer = 100

Private Const PPFileTitle As String = "data.ini"

Private PPFilePath As String

Private Function Trim0(str1 As String) As String
    Dim n As Integer
    n = InStr(1, str1, vbNullChar)
    If n > 1 Then
        Trim0 = Left$(str1, n - 1)
    Else
        Trim0 = ""
    End If
        
End Function

Public Sub Init_PPFile()
    Dim p As String
    
    p = App.Path
    If Right$(p, 1) <> "\" Then
        p = p & "\"
    End If
    
    PPFilePath = p & PPFileTitle
    
End Sub

Public Function GPPS(strAPP As String, strKEY As String, strDft As String, strDat As String) As Long

    Dim s As String * STRING_LENGTH

    GPPS = GetPrivateProfileString(strAPP, strKEY, strDft, s, STRING_LENGTH, PPFilePath)
    
    strDat = Trim0(s)

End Function

Public Function WPPS(strAPP As String, strKEY As String, strDat As String) As Long
    
    WPPS = WritePrivateProfileString(strAPP, strKEY, strDat, PPFilePath)
    
End Function
