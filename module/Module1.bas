Attribute VB_Name = "Module1"
Option Explicit
'**************************************************
'** グローバル関数
'**************************************************

Public Sub Inc(ByRef v As Integer)
    v = v + 1
End Sub

'**************************************************
'日付形式(例:2005/01/01)をLong型(例:20050101)に変換する
'
Public Function DateToLong(dt) As Long
    DateToLong = CLng(Year(dt)) * 10000& _
                + CLng(Month(dt)) * 100& _
                + CLng(Day(dt))
End Function

'**************************************************
'Long型(例:20050101)を日付形式(例:2005/01/01)に変換する
'
Public Function LongToDate(d As Long)
    LongToDate = DateSerial(CInt(d \ 10000&), _
                            CInt((d Mod 10000&) \ 100&), _
                            CInt(d Mod 100&))
End Function

'**************************************************
'唯一無二のファイル名を作る（重複した場合に連番付けする）
'
Public Function GetOnlyName( _
                        fso1 As Scripting.FileSystemObject, _
                        str1 As String, _
                        strFolder As String, _
                        Optional strExtension As String = "txt") As String
    Dim pp As Integer
    Dim strFile2 As String
    
    strFile2 = fso1.BuildPath(strFolder, str1 & "." & strExtension)
    pp = 2
    Do While (fso1.FileExists(strFile2))
        strFile2 = fso1.BuildPath(strFolder, _
                    str1 & "(" & Format(pp) & ")" & "." & strExtension)
        pp = pp + 1
    Loop
    GetOnlyName = strFile2
End Function

'**************************************************
'レート計算
Public Function CalcRating(sngRate As Single, lngPlayCount As Long, _
                            intRank As Integer, _
                            sngAvgRate As Single, blnSanma As Boolean) As Single
    Dim sngPCHosei  As Single
    Dim sngHosei    As Single
    Dim sngKekka    As Single
    Dim sngHendo    As Single
    Dim sngNewRate  As Single
    
    If lngPlayCount < 400 Then
        sngPCHosei = 1! - CSng(lngPlayCount) * 0.002!
    Else
        sngPCHosei = 0.2!
    End If
    
    sngHosei = (sngAvgRate - sngRate) / 40!
    
    If blnSanma Then
        sngKekka = IIf(intRank = 0, 30!, IIf(intRank = 1, 0!, -30!))
    Else
        sngKekka = IIf(intRank = 0, 30!, IIf(intRank = 1, 10!, _
                        IIf(intRank = 2, -10!, -30!)))
    End If
    
    sngHendo = sngPCHosei * (sngKekka + sngHosei)
    
    sngNewRate = sngRate + sngHendo
    
    CalcRating = Int(100! * sngNewRate + 0.999999!) / 100!
    
End Function

'**************************************************
'段位別昇段判定
Private Function CheckDanUp(Dan As Long, pt As Long) As Boolean
    '▼段位別昇段判定
    Select Case Dan
    Case 0, 1, 2    '新人９級８級
        CheckDanUp = pt >= 30
    Case 3, 4, 5    '７級６級５級
        CheckDanUp = pt >= 60
    Case 6          '４級
        CheckDanUp = pt >= 90
    Case 7, 8, 9    '３級２級１級
        CheckDanUp = pt >= 100
    Case Else       '初段以上
        CheckDanUp = pt >= ((Dan - 9&) * 400&)
    End Select
End Function

'**************************************************
'ポイント段位値に順位(0〜3)に基づきポイント値を加算する
'ポイント段位値は下二桁が段位値で残りの桁がポイント値になる
'天鳳の段位制準拠で一般卓のポイント値で段位変動させる
'（３麻と４麻は同じポイント値変動なのでblnSambaは順位切りでしか使わない）
'初期値は 0
Public Function ChangeDanPoint(lngDanPoint As Long, intRank As Integer, _
                                   blnNanba As Boolean, blnSanma As Boolean, _
                                   blnJoTaku As Boolean) As Long

    '▼ポイント変動無い場合は早々にご退場
    If ((blnSanma = True) And (intRank = 1)) _
            Or ((blnSanma = False) And (intRank = 2)) Then
        '▼３麻２位もしくは４麻３位
        ChangeDanPoint = lngDanPoint
        Exit Function
    End If

    Dim Dan     As Long
    Dim pt      As Long
    
    '▼段位値とポイントを分ける
    Dan = lngDanPoint Mod 100&
    pt = (lngDanPoint - Dan) \ 100&
    
    
    If intRank = 0 Then
        '▼１位（トップ）
        pt = pt + IIf(blnJoTaku, IIf(blnSanma, 5&, 4&), 3&) * IIf(blnNanba, 15, 10)
        
        '▼段位別昇段判定
        If CheckDanUp(Dan, pt) = True Then
            If Dan < 20 Then
                Dan = Dan + 1
            End If
            pt = -1
        End If

    
    ElseIf intRank = 1 Then
        '▼４麻２位
        
        '▼一般卓相当ならポイント変動なし
        If blnJoTaku = False Then
            ChangeDanPoint = lngDanPoint
            Exit Function
        End If
        
        pt = pt + 1& * IIf(blnNanba, 15, 10)
        
        '▼段位別昇段判定
        If CheckDanUp(Dan, pt) = True Then
            If Dan < 20 Then
                Dan = Dan + 1
            End If
            pt = -1
        End If

        
    Else
        '▼最下位（ラス）
        
        '▼２級未満は変動なし
        If Dan < 8& Then
            ChangeDanPoint = lngDanPoint
            Exit Function
        End If
        
        pt = pt - (Dan - 7&) * IIf(blnNanba, 15&, 10&)
        
        '▼初段以上でポイント値マイナスなったら降段
        If (Dan >= 10) And (pt < 0) Then
            Dan = Dan - 1
        End If
        
    End If
    
    '▼ポイント値がマイナスならポイント値を各段位の初期値に再設定
    If pt < 0 Then
        If Dan < 10 Then
            '▼１級以下
            pt = 0
        Else
            '▼初段以上
            pt = (Dan - 9&) * 200&
        End If
    End If

    '▼計算されたポイント段位値を返す
    ChangeDanPoint = pt * 100& + Dan

End Function

