VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   3  '固定ﾀﾞｲｱﾛｸﾞ
   Caption         =   "設定"
   ClientHeight    =   3735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8010
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3735
   ScaleWidth      =   8010
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  '画面の中央
   Begin VB.Frame Frame3 
      Caption         =   "数値等の設定"
      Height          =   3135
      Left            =   4080
      TabIndex        =   18
      Top             =   120
      Width           =   3735
      Begin VB.TextBox Text2 
         Height          =   270
         Left            =   1680
         TabIndex        =   22
         Text            =   "50"
         Top             =   840
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   270
         Left            =   1680
         TabIndex        =   19
         Top             =   360
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "段位リスト最小対戦数"
         Height          =   375
         Left            =   240
         TabIndex        =   21
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "今日の日付"
         Height          =   180
         Left            =   240
         TabIndex        =   20
         Top             =   360
         Width           =   900
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "出力情報"
      Height          =   3135
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   3855
      Begin VB.CheckBox Check1 
         Caption         =   "時間別対戦頻度"
         Height          =   255
         Index           =   13
         Left            =   2040
         TabIndex        =   17
         Top             =   2400
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "段位リスト"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   16
         Top             =   2400
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "ルール別対戦数"
         Height          =   255
         Index           =   10
         Left            =   2040
         TabIndex        =   14
         Top             =   2040
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "3ヶ月〜6ヶ月参戦者"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   15
         Top             =   2040
         Value           =   1  'ﾁｪｯｸ
         Width           =   1935
      End
      Begin VB.CheckBox Check1 
         Caption         =   "プロフィル"
         Height          =   255
         Index           =   9
         Left            =   2040
         TabIndex        =   13
         Top             =   1680
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "全員名簿"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   12
         Top             =   1680
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check2 
         Caption         =   "全て"
         Height          =   255
         Left            =   2040
         TabIndex        =   11
         Top             =   2760
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "３麻同卓"
         Height          =   255
         Index           =   7
         Left            =   2040
         TabIndex        =   10
         Top             =   1320
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "４麻同卓"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   9
         Top             =   1320
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "３麻詳細Ａ"
         Height          =   255
         Index           =   5
         Left            =   2040
         TabIndex        =   8
         Top             =   960
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "４麻詳細Ａ"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   7
         Top             =   960
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "新参数カウント"
         Height          =   255
         Index           =   3
         Left            =   2040
         TabIndex        =   6
         Top             =   600
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "メンバー数"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "3ヶ月参戦者"
         Height          =   255
         Index           =   1
         Left            =   2040
         TabIndex        =   4
         Top             =   240
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
      Begin VB.CheckBox Check1 
         Caption         =   "各年参戦者"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Value           =   1  'ﾁｪｯｸ
         Width           =   1695
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   255
      Left            =   5520
      TabIndex        =   1
      Top             =   3360
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   255
      Left            =   6720
      TabIndex        =   0
      Top             =   3360
      Width           =   1095
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Event MyEvent()

Dim mdicChecks   As Scripting.Dictionary
Dim mintBackup() As Integer
Dim mDate        As String

Private Sub BackUp()
    Dim i%
    For i = 0 To Check1.Count - 1
        mintBackup(i) = Check1(i).Value
    Next i
    mDate = Text1.Text
End Sub

Private Sub CheckDate()
    Dim d As Date
    If IsDate(Text1.Text) Then
        d = CDate(Text1.Text)
    Else
        d = Now
    End If
    Text1.Text = DateSerial(Year(d), Month(d), Day(d))
End Sub

Public Property Get NowDate() As Date
    Call CheckDate
    NowDate = CDate(Text1.Text)
End Property

Public Property Get DataOutput(strKey As String) As Boolean
    If mdicChecks.Exists(strKey) Then
        Dim c As CheckBox
        Set c = mdicChecks.Item(strKey)
        DataOutput = (c.Value = vbChecked)
    Else
        DataOutput = True
    End If
End Property

Public Property Get NumberData(strKey As String) As Long
    If strKey = "段位リスト最小対戦数" Then
        NumberData = CLng(Text2.Text)
    End If
End Property

Private Sub ResetChecks()
    Dim i%
    For i = 0 To Check1.Count - 1
        Check1(i).Value = mintBackup(i)
    Next i
    Text1.Text = mDate
End Sub

Private Sub Check1_Click(Index As Integer)
    Dim n%, c
    n = 0
    For Each c In Check1
        If c.Value = vbChecked Then
            n = n + 1
        End If
    Next c
    
    If n = 0 Then
        If Check2.Value <> vbUnchecked Then
            Check2.Value = vbUnchecked
        End If
    ElseIf n = Check1.Count Then
        If Check2.Value <> vbChecked Then
            Check2.Value = vbChecked
        End If
    Else
        If Check2.Value <> vbGrayed Then
            Check2.Value = vbGrayed
        End If
    End If
    
End Sub

Private Sub Check2_Click()
    Dim c
    If Check2.Value = vbChecked Then
        For Each c In Check1
            If c.Value = vbUnchecked Then
                c.Value = vbChecked
            End If
        Next c
    ElseIf Check2.Value = vbUnchecked Then
        For Each c In Check1
            If c.Value = vbChecked Then
                c.Value = vbUnchecked
            End If
        Next c
    End If
End Sub

Private Sub cmdCancel_Click()
    Call ResetChecks
    RaiseEvent MyEvent
End Sub

Private Sub cmdOK_Click()
    Call CheckDate
    Call BackUp
    gvarDateNow = Text1.Text
    RaiseEvent MyEvent
End Sub

Private Sub Form_Initialize()
    Set mdicChecks = New Scripting.Dictionary
End Sub

Private Sub Form_Load()
    ReDim mintBackup(Check1.Count - 1)
    Dim i%
    For i = 0 To Check1.Count - 1
        mdicChecks.Add Check1(i).Caption, Check1(i)
        Check1(i).Value = vbChecked
    Next i
    Call CheckDate
    Call BackUp
    Label2.Caption = "段位リスト" & vbNewLine & "最小対戦数"
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Then
        Call ResetChecks
        RaiseEvent MyEvent
        Cancel = 1
    End If
    
End Sub

Private Sub Form_Terminate()
    mdicChecks.RemoveAll
    Set mdicChecks = Nothing
End Sub

