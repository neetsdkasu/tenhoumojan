VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.OCX"
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "天鳳個室ログからカウント"
   ClientHeight    =   3375
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3375
   ScaleWidth      =   6015
   StartUpPosition =   2  '画面の中央
   Begin VB.CommandButton cmdCounting 
      Caption         =   "Counting"
      Height          =   375
      Left            =   3480
      TabIndex        =   11
      Top             =   2880
      Width           =   2415
   End
   Begin MSComDlg.CommonDialog dlgOpen 
      Left            =   720
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Filter          =   "テキストファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*"
   End
   Begin VB.Frame fraContainer 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6015
      Begin VB.CommandButton cmdOutput 
         Caption         =   "Output"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   2880
         Width           =   2415
      End
      Begin VB.ListBox List1 
         Enabled         =   0   'False
         Height          =   1860
         Index           =   1
         Left            =   120
         MultiSelect     =   2  '拡張
         OLEDropMode     =   1  '手動
         TabIndex        =   9
         Top             =   240
         Visible         =   0   'False
         Width           =   5775
      End
      Begin VB.CommandButton cmdDown 
         Caption         =   "Down"
         Height          =   255
         Left            =   2760
         TabIndex        =   8
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdUp 
         Caption         =   "Up"
         Height          =   255
         Left            =   2040
         TabIndex        =   7
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   2160
         Width           =   855
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Height          =   255
         Left            =   1080
         TabIndex        =   4
         Top             =   2160
         Width           =   855
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "Add"
         Height          =   255
         Left            =   4680
         TabIndex        =   3
         Top             =   2160
         Width           =   1215
      End
      Begin VB.CommandButton cmdListChange 
         Caption         =   "Change"
         Height          =   255
         Left            =   3480
         TabIndex        =   2
         Top             =   2160
         Width           =   1095
      End
      Begin VB.ListBox List1 
         Height          =   1860
         Index           =   0
         Left            =   120
         MultiSelect     =   2  '拡張
         OLEDropMode     =   1  '手動
         TabIndex        =   1
         Top             =   240
         Width           =   5775
      End
      Begin VB.Label lblState 
         Alignment       =   2  '中央揃え
         Height          =   255
         Left            =   3480
         TabIndex        =   10
         Top             =   2520
         Width           =   2415
      End
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   120
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "txt"
      FileName        =   "フォルダ"
      Filter          =   "テキストファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*"
   End
   Begin VB.Menu mnuSort 
      Caption         =   "ソート"
      Begin VB.Menu mnuSortSort 
         Caption         =   "ASCIIコード昇順"
         Index           =   0
      End
      Begin VB.Menu mnuSortSort 
         Caption         =   "ASCIIコード降順"
         Index           =   1
      End
      Begin VB.Menu mnuSortSort 
         Caption         =   "五十音昇順"
         Index           =   2
      End
      Begin VB.Menu mnuSortSort 
         Caption         =   "五十音降順"
         Index           =   3
      End
   End
   Begin VB.Menu mnuData 
      Caption         =   "データ"
      Begin VB.Menu mnuData_PlayersCount 
         Caption         =   "人数"
      End
      Begin VB.Menu mnuData_bar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuData_Clear 
         Caption         =   "クリア"
      End
   End
   Begin VB.Menu mnuSetting 
      Caption         =   "設定"
      Begin VB.Menu mnuSetting_Setting 
         Caption         =   "設定"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents wcls3 As Class3
Attribute wcls3.VB_VarHelpID = -1

Private WithEvents wfrm2 As Form2
Attribute wfrm2.VB_VarHelpID = -1

Dim mdicPlayers As Scripting.Dictionary
' Key プレイヤー名
' Item　Class1のプレイヤーデータ

Dim mdicPlay2Players As Scripting.Dictionary

Dim mdicRulePlayCount As Scripting.Dictionary

Dim mdicDate    As Scripting.Dictionary     '同一日の集計をしないため
' Key 日付 (sca********.logの日付部)
' Item 同上

'**************************************************
'
Private Sub OutputData(strFile As String)

    Me.Caption = "出力中..."
    fraContainer.Enabled = False
    Screen.MousePointer = vbHourglass
    
    lblState.Caption = "Waiting..."
    DoEvents
    
    '*****************
    
    Dim cls3 As Class3
    Set cls3 = New Class3
    Set wcls3 = cls3
    
    Dim NowDate As Date
    
    NowDate = wfrm2.NowDate
    
    cls3.Init mdicPlayers, strFile, NowDate

    If wfrm2.DataOutput("プロフィル") Then
        cls3.OutputProfileList
    End If

    '*****************
    '各年参戦者
    If wfrm2.DataOutput("各年参戦者") Then
        cls3.OutputMemberListAtYear
    End If
    
    '**********************
    '3ヶ月参戦者
    If wfrm2.DataOutput("3ヶ月参戦者") Then
        cls3.OutputLast3MonthPlay
    End If
    
    '**********************
    '3ヶ月〜6ヶ月参戦者
    If wfrm2.DataOutput("3ヶ月〜6ヶ月参戦者") Then
        cls3.OutputLast3to6MonthPlay
    End If
    
    '*************
    'メンバー数
    If wfrm2.DataOutput("メンバー数") Then
        cls3.OutputMembersCount
    End If

    '****************
    '全員名簿
    If wfrm2.DataOutput("全員名簿") Then
        cls3.OutputAllMenberList
    End If
    
    '****************
    '新参数カウント
    If wfrm2.DataOutput("新参数カウント") Then
        cls3.OutputNewFaceCount
    End If
    
    '****************
    '100戦以上者詳細４麻のみ
    If wfrm2.DataOutput("４麻詳細Ａ") Then
        cls3.OutputDetailA4p
    End If
    
    '****************
    '50戦以上者詳細３麻のみ
    If wfrm2.DataOutput("３麻詳細Ａ") Then
        cls3.OutputDetailA3p
    End If

    '*****************
    '４麻同卓
    If wfrm2.DataOutput("４麻同卓") Then
        cls3.Output2Players mdicPlay2Players, 0
    End If
    
    '*****************
    '３麻同卓
    If wfrm2.DataOutput("３麻同卓") Then
        cls3.Output2Players mdicPlay2Players, 1, 10
    End If
    
    '*****************
    'ルール別対戦数
    If wfrm2.DataOutput("ルール別対戦数") Then
        cls3.OutputRuleCount mdicRulePlayCount
    End If
    
    '*****************
    '段位リスト
    If wfrm2.DataOutput("段位リスト") Then
        cls3.OutputDanList wfrm2.NumberData("段位リスト最小対戦数")
    End If
    
    '*****************
    '時間別対戦数
    If wfrm2.DataOutput("時間別対戦頻度") Then
        cls3.OutputTimePlayCount
    End If
    
    
    '*****************
    Set wcls3 = Nothing
    
    cls3.Release
    Set cls3 = Nothing
    
    '*****************
    
    lblState.Caption = "Complete"
    Screen.MousePointer = vbDefault
    fraContainer.Enabled = True
    Me.Caption = "天鳳個室ログからカウント (" & Format(mdicDate.Count) & ")"
    
End Sub

'**************************************************
'
Private Sub DataCounting()
    Dim fso1 As Scripting.FileSystemObject
    Dim txs2 As Scripting.TextStream
    
    Set fso1 = New Scripting.FileSystemObject
    
    Dim i%, c%, str1$, j%, k%, jtk%, avgRating!, df&, dp&
    Dim lngDate&, p1&, p2&, p3&, p4&, f1%, f2%, hr%
    Dim str2$, v, v2, str3$, strMb$(), intSc%(), sngPW!()
    Dim strTime$, strRule$, str4$, intWeek%
    Dim cls1 As Class1, cls1b As Class1
    Dim cls2 As Class2
    
    Dim dicTemp As Scripting.Dictionary
    
    glngDate3MonthAgo = DateToLong(DateAdd("m", -3, gvarDateNow))
    Debug.Print glngDate3MonthAgo
    
    Dim ruleCount&
    
    c = List1(0).ListCount
    
    lngDate = 0&
    
    '▼外観
    fraContainer.Enabled = False
    Screen.MousePointer = vbHourglass
    Me.Caption = "カウント中..."
    
    intWeek = 1
    
    '▼全てのファイルを走査するよ
    For i = 0 To c - 1
        
        '▼ファイルを読み込む
        Set txs2 = fso1.OpenTextFile(List1(0).List(i), , , TristateUseDefault)
        
        '▼ファイルの終わりまでループしてね
        Do While (txs2.AtEndOfStream = False)
            
            '▼１行ゲット
            str1 = txs2.ReadLine()
            
            If str1 Like "sca*.log" Then
                
                '▼日付発見！
                lngDate = CLng(Mid(str1, 4, 8))
                If mdicDate.Exists(Str(lngDate)) Then
                    lngDate = 0&
                Else
                    mdicDate.Add Str(lngDate), Str(lngDate)
                
                End If
                intWeek = Weekday(LongToDate(lngDate), vbMonday)
                
            ElseIf lngDate > 0& Then
                
                '▼ログだよ。区分けと時刻・ルール取得
                p1 = InStr(1, str1, "|")
                p2 = InStr(p1 + 1, str1, "|")
                p3 = InStr(p2 + 1, str1, "|")
                str2 = Trim(Right(str1, Len(str1) - p3))
                v = Split(str2, " ")
                strTime = Trim(Mid(str1, p1 + 1, p2 - p1 - 1))
                strRule = Trim(Mid(str1, p2 + 1, p3 - p2 - 1))
                
                If lngDate >= glngDate3MonthAgo Then
                    hr = Hour(strTime)
                    Inc gintTimePlayCount(0, hr)
                    Inc gintTimePlayCount(0, 24)
                    Inc gintTimePlayCount(intWeek, hr)
                    Inc gintTimePlayCount(intWeek, 24)
                    If intWeek < 6 Then
                        Inc gintTimePlayCount(8, hr)
                        Inc gintTimePlayCount(8, 24)
                    Else
                        Inc gintTimePlayCount(9, hr)
                        Inc gintTimePlayCount(9, 24)
                    End If
                End If
                
                
                '▼旧式のルール表示を新式に直す
                If Len(strRule) = 5 Then
                    strRule = strRule & "−"
                End If
                
                '▼ルール別カウントデータ格納庫を取得
                With mdicRulePlayCount
                    If .Exists(strRule) Then
                        Set dicTemp = .Item(strRule)
                        'ruleCount = CLng(.Item(strRule)) + 1
                    Else
                        Set dicTemp = New Scripting.Dictionary
                        .Add strRule, dicTemp
                        'ruleCount = 1
                    End If
                End With
                '▼ルール別カウントデータの更新
                With dicTemp
                    If .Exists("Total") Then
                        ruleCount = CLng(.Item("Total")) + 1
                    Else
                        ruleCount = 1
                    End If
                    .Item("Total") = ruleCount
                    str3 = Left(CStr(lngDate), 4)
                    If .Exists(str3) Then
                        ruleCount = CLng(.Item(str3)) + 1
                    Else
                        ruleCount = 1
                    End If
                    .Item(str3) = ruleCount
                End With
                
                '▼各人の名前と点数を順位順に取得（jが順位だよ）
                jtk = 0
                avgRating = 0!
                ReDim strMb(UBound(v))
                ReDim intSc(UBound(v))
                ReDim sngPW(UBound(v))
                For j = 0 To UBound(v)
                    str3 = CStr(v(j))
                    p4 = InStrRev(str3, "(")
                    strMb(j) = Left(str3, p4 - 1)
                    intSc(j) = CInt(Mid(str3, p4 + 1, Len(str3) - p4 - 1))
                    If (strMb(j) <> "NoName") And (mdicPlayers.Exists(strMb(j))) Then
                        Set cls1 = mdicPlayers.Item(strMb(j))
                        If cls1.Dan(3 - UBound(v)) >= 9 Then
                            jtk = jtk + 1
                        End If
                        avgRating = avgRating + cls1.Rating(3 - UBound(v))
                    Else
                        avgRating = avgRating + 1500!
                    End If
                Next j
                avgRating = avgRating / CSng(UBound(v) + 1)
                If avgRating < 1500! Then
                    avgRating = 1500!
                End If
                
                '▼各人のデータを更新（jは順位だよ）
                For j = 0 To UBound(v)
                
                    '▼個人データ格納庫を取得
                    If mdicPlayers.Exists(strMb(j)) Then
                        Set cls1 = mdicPlayers.Item(strMb(j))
                    Else
                        Set cls1 = New Class1
                        cls1.SetName strMb(j)
                        mdicPlayers.Add strMb(j), cls1
                    End If
                    
                    '▼個人データを更新
                    cls1.AddData _
                            lngDate:=lngDate, _
                            strTime:=strTime, _
                            intRank:=j, _
                            intScore:=intSc(j), _
                            strRule:=strRule, _
                            blnJoTaku:=(jtk = 4), _
                            sngAvgRating:=avgRating, _
                            intWeek:=intWeek

                    '▼２者間のデータを更新（重複無いようにしてるよ）
                    For k = j + 1 To UBound(v)
                    

                        
                    
                        '▼名無しさんは省いてね
                        If (strMb(j) <> "NoName") And (strMb(k) <> "NoName") Then
                            
                            '▼名前順に並べて格納キーを生成
                            If StrComp(strMb(j), strMb(k)) > 0 Then
                                f1 = k
                                f2 = j
                            Else
                                f1 = j
                                f2 = k
                            End If
                            str4 = strMb(f1) & " " & strMb(f2)
                            
                            '▼２者間のデータを取得・更新
                            If mdicPlay2Players.Exists(str4) Then
                                Set cls2 = mdicPlay2Players.Item(str4)
                            Else
                                Set cls2 = New Class2
                                cls2.SetPlayer strMb(f1), strMb(f2)
                                mdicPlay2Players.Add str4, cls2
                            End If
                            cls2.AddData strRule, intSc(f1), intSc(f2), f1, f2
                            
                        End If
                        
                    Next k
                    
                Next j
                
                For j = 0 To UBound(v)
                    Set cls1 = mdicPlayers.Item(strMb(j))
                    For k = j + 1 To UBound(v)
                        Select Case k - j
                        Case 1
                            dp = 40&
                        Case 2
                            dp = 60&
                        Case 3
                            dp = 100&
                        End Select
                        
                        Set cls1b = mdicPlayers.Item(strMb(k))
                        df = CLng(Abs(cls1.Power(3 - UBound(v)) - cls1b.Power(3 - UBound(v)))) \ 1000&
                        If df >= 10& Then
                            sngPW(j) = sngPW(j) + CSng(dp) * 0.01!
                            sngPW(k) = sngPW(k) - CSng(dp) * 0.01!
                        ElseIf df < 5& Then
                            sngPW(j) = sngPW(j) + CSng(dp) * (1! - CSng(df) * 0.2!)
                            sngPW(k) = sngPW(k) - CSng(dp) * (1! - CSng(df) * 0.2!)
                        Else
                            sngPW(j) = sngPW(j) + CSng(dp) * (0.1! - CSng(df - 5&) * 0.02!)
                            sngPW(k) = sngPW(k) - CSng(dp) * (0.1! - CSng(df - 5&) * 0.02!)
                        End If
                    Next k
                Next j
                For j = 0 To UBound(v)
                    If strMb(j) <> "NoName" Then
                        Set cls1 = mdicPlayers.Item(strMb(j))
                        cls1.AddPower 3 - UBound(v), CLng(sngPW(j) + 0.99999!)
                    End If
                Next j
                
            End If
            
            '▼進捗状況おしらせ
            lblState.Caption = Str(txs2.Line) & "/" & Str(i + 1) & "/" & Str(c)
            DoEvents
            
        Loop
            
    
    Next i

    '▼終わりの処理

    '▼外観
    lblState.Caption = "Complete"

    fraContainer.Enabled = True
    Screen.MousePointer = vbDefault
    Me.Caption = "天鳳個室ログからカウント (" & Format(mdicDate.Count) & ")"

    '▼メモリ解放？
    Set fso1 = Nothing
    Set txs2 = Nothing
    
End Sub

'**************************************************
'
Private Sub ListAdd(strFile As String)
    On Error Resume Next
    
    Dim str1$, i&
    For i = 0 To List1(0).ListCount
        str1 = List1(0).List(i)
        If str1 = strFile Then
            Exit Sub
        End If
    Next i
    List1(0).AddItem strFile
    Dim fso1 As Scripting.FileSystemObject
    Set fso1 = New Scripting.FileSystemObject
    List1(1).AddItem fso1.GetFileName(strFile)
    Set fso1 = Nothing
End Sub

'**************************************************
'
Private Sub ListSelectedCopy()
    On Error Resume Next
    
    Dim i&, p&
    p = IIf(List1(0).Visible, 0, 1)
    For i = 0 To List1(p).ListCount - 1
        List1(1 - p).Selected(i) = List1(p).Selected(i)
    Next i
End Sub

'//////////////////////////////////////////////////
'
Private Sub cmdAdd_Click()
    On Error Resume Next
    
    dlgOpen.Flags = MSComDlg.FileOpenConstants.cdlOFNFileMustExist _
            Or MSComDlg.FileOpenConstants.cdlOFNPathMustExist _
            Or MSComDlg.FileOpenConstants.cdlOFNLongNames _
            Or MSComDlg.FileOpenConstants.cdlOFNHideReadOnly
    
    Err.Clear
    
    dlgOpen.ShowOpen
    
    If Err.Number = MSComDlg.ErrorConstants.cdlCancel Then
        Exit Sub
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Description, vbCritical Or vbOKOnly, Str(Err.Number)
        Exit Sub
    End If
    
    ListAdd dlgOpen.FileName
        
End Sub

'//////////////////////////////////////////////////
'
Private Sub cmdClear_Click()
    On Error Resume Next
    
    If List1(0).ListCount = 0 Then Exit Sub
    If MsgBox("リストをクリアします", vbInformation Or vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    List1(0).Clear
    List1(1).Clear
End Sub

'//////////////////////////////////////////////////
'
Private Sub cmdCounting_Click()
    Call DataCounting
End Sub

Private Sub cmdDelete_Click()
    On Error Resume Next
    
    If List1(0).ListCount = 0 Then Exit Sub
    If MsgBox("選択されている項目を削除します", vbInformation Or vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    Dim i&, c&
    c = List1(0).ListCount
    ListSelectedCopy
    For i = c - 1 To 0 Step -1
        If List1(0).Selected(i) Then
            List1(0).RemoveItem i
            List1(1).RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdDown_Click()
    On Error Resume Next
    
    Dim i&, c&, str1$, lst1 As ListBox
    c = List1(0).ListCount
    If c < 2 Then Exit Sub
    ListSelectedCopy
    Dim k&
    k = IIf(List1(0).Selected(c - 1), c - 1, c)
    For i = c - 2 To 0 Step -1
        If List1(0).Selected(i) Then
            If k - i = 1 Then
                k = i
            Else
                For Each lst1 In List1
                    str1 = lst1.List(i)
                    lst1.RemoveItem i
                    lst1.AddItem str1, i + 1
                    lst1.Selected(i + 1) = True
                Next lst1
            End If
        End If
    Next i
End Sub

Private Sub cmdListChange_Click()
    On Error Resume Next
    
    Dim lst1 As ListBox
    ListSelectedCopy
    For Each lst1 In List1
        With lst1
            .Visible = Not (.Visible)
            .Enabled = Not (.Enabled)
        End With
    Next lst1
End Sub

Private Sub cmdOutput_Click()
    Dim strFile$
    
    If List1(0).ListCount = 0 Then Exit Sub
    
    dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNHideReadOnly _
                Or MSComDlg.FileOpenConstants.cdlOFNPathMustExist _
                Or MSComDlg.FileOpenConstants.cdlOFNOverwritePrompt
    
    On Error GoTo ERROR_cmdOutput_Click
    
    Err.Clear
    
    dlgSave.ShowSave
    
    OutputData dlgSave.FileName
        
    Exit Sub
    
ERROR_cmdOutput_Click:
    If Err.Number <> MSComDlg.ErrorConstants.cdlCancel Then
        MsgBox Err.Description, vbCritical Or vbOKOnly, Str(Err.Number)
        Debug.Print "ERROR "; Err.Number, Err.Description
        Screen.MousePointer = vbDefault
    End If
End Sub


Private Sub cmdUp_Click()
    On Error Resume Next
    
    Dim i&, c&, str1$, lst1 As ListBox
    c = List1(0).ListCount
    If c < 2 Then Exit Sub
    ListSelectedCopy
    Dim k&
    k = IIf(List1(0).Selected(0), 0, -1)
    For i = 1 To c - 1
        If List1(0).Selected(i) Then
            If i - k = 1 Then
                k = i
            Else
                For Each lst1 In List1
                    str1 = lst1.List(i)
                    lst1.RemoveItem i
                    lst1.AddItem str1, i - 1
                    lst1.Selected(i - 1) = True
                Next lst1
            End If
        End If
    Next i
End Sub

Private Sub Form_Load()
    
    ReDim gintTimePlayCount(9, 24)
    
    gstrDanName = Split("新人/９級/８級/７級/６級/５級/４級/３級/２級/１級/" _
                       & "初段/二段/三段/四段/五段/六段/七段/八段/九段/十段/天鳳", "/")
    
    Set mdicPlayers = New Scripting.Dictionary
    Set mdicPlay2Players = New Scripting.Dictionary
    Set mdicDate = New Scripting.Dictionary
    
    Set mdicRulePlayCount = New Scripting.Dictionary
    
    Set wfrm2 = New Form2
    
    Set gcPPS = New clsPrivateProfileString
    
    gvarDateNow = DateSerial(Year(Now), Month(Now), Day(Now))
    
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    
    If fraContainer.Enabled = False Then
        If MsgBox("作業中ですが終了しますか？", vbInformation Or vbYesNo, "確認") = vbNo Then
            Cancel = 1
        End If
    End If
End Sub

Private Sub List1_Click(Index As Integer)
    With List1(0)
        .ToolTipText = .List(.ListIndex)
    End With
End Sub

Private Sub List1_OLEDragDrop(Index As Integer, Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error Resume Next
    
    If Data.GetFormat(vbCFFiles) = False Then
        Effect = vbDropEffectNone
        Exit Sub
    End If
    Dim str1
    For Each str1 In Data.Files
        ListAdd CStr(str1)
    Next str1
End Sub



Private Sub mnuData_Clear_Click()
    If MsgBox("データをクリアします", vbInformation Or vbYesNoCancel, "確認") _
                <> vbYes Then
        Exit Sub
    End If
    Set mdicPlayers = Nothing
    Set mdicDate = Nothing
    Set mdicPlayers = New Scripting.Dictionary
    Set mdicDate = New Scripting.Dictionary
    Set mdicRulePlayCount = New Scripting.Dictionary
    Me.Caption = "天鳳個室ログからカウント"
End Sub

Private Sub mnuData_PlayersCount_Click()
    Dim v
    Dim cls1 As Class1
    Dim c3%, c4%
    
    For Each v In mdicPlayers.Items()
        Set cls1 = v
        c4 = c4 + IIf(cls1.PlayCount(0) > 0, 1, 0)
        c3 = c3 + IIf(cls1.PlayCount(1) > 0, 1, 0)
    Next v

    MsgBox "現在の人数" & vbNewLine _
            & "４麻(" & Str(c4) & " )" & vbNewLine _
            & "３麻(" & Str(c3) & " )" & vbNewLine _
            & "トータル(" & Str(mdicPlayers.Count) & " )", , "データ"
End Sub

Private Sub mnuSetting_Setting_Click()
    wfrm2.Show vbModeless, Me
    Me.Enabled = False
End Sub

Private Sub mnuSortSort_Click(Index As Integer)
    
    If List1(0).ListCount < 2 Then Exit Sub
    
    Dim p%, c%
    p = IIf(List1(0).Visible, 0, 1)
    c = List1(0).ListCount
    
    Dim lst1 As ListBox
    Dim i%, j%, F%, str1$
    
    For j = 0 To c
        For i = 0 To c - 2
            F = 0
            Set lst1 = List1(p)
            Select Case Index
            Case 0
                With lst1
                    If StrComp(.List(i), .List(i + 1)) > 0 Then
                        F = 1
                    End If
                End With
            Case 1
                With lst1
                    If StrComp(.List(i), .List(i + 1)) < 0 Then
                        F = 2
                    End If
                End With
            Case 2
                With lst1
                    If StrComp(.List(i), .List(i + 1), vbTextCompare) > 0 Then
                        F = 3
                    End If
                End With
            Case Else
                With lst1
                    If StrComp(.List(i), .List(i + 1), vbTextCompare) < 0 Then
                        F = 4
                    End If
                End With
            End Select
            If F > 0 Then
                For Each lst1 In List1
                    str1 = lst1.List(i)
                    lst1.RemoveItem i
                    lst1.AddItem str1, i + 1
                Next lst1
            End If
        Next i
    Next j
    
End Sub

Private Sub mnuStop_Click()
    On Error Resume Next
    
    If fraContainer.Enabled = False Then
        If MsgBox("作業を中止しますか？", vbInformation Or vbYesNo, "確認") = vbNo Then
            Exit Sub
        End If
    End If
End Sub

Private Sub wcls3_Progress(strProgress As String)
    lblState.Caption = strProgress
    DoEvents
End Sub

Private Sub wfrm2_MyEvent()
    Debug.Print "MyEvent"
    wfrm2.Visible = False
    Me.Enabled = True
    Me.SetFocus
End Sub
